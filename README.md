![](/docs/logo/PT_WebCore.svg)

Pt is a Object Oriented PHP Web Core that Focus is to provide a Platform to develop Web Applications With Web Front End and RESTful API.


The framework Follows and A "Pay For What You Use" Model where entry points of the program only import the parts of library they need to work similarly to C/C++. 

It is our Philosophy to ignore templateing systems and just output Raw HTML/TEXT from reading or inducing an HTML file.

Built on top of Technology like MariaDB, MySQLi, PHP(ZEND)/HHVM...




USES
mysqli
argon2
bcmath
gmp
