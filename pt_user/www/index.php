<?php

include_once("../lib/url/URLPath.php");

/* URL Router */
$url_path = new URLPath();
if($url_path->assign('/activate','activate.php'));
else if($url_path->assign('/deactivate','deactivate.php'));
else if($url_path->assign('/home','home.php'));
else if($url_path->assign('/location','location.php'));
else if($url_path->assign('/login','login.php'));
else if($url_path->assign('/logout','logout.php'));
else if($url_path->assign('/password','password.php'));
else if($url_path->assign('/recover','recover.php'));
else if($url_path->assign('/search','search.php'));
else if($url_path->assign('/signup','signup.php'));
else if($url_path->assign('/upload','upload.php'));
else if($url_path->assign('/user','user.php'));
else
{
	if($url_path->get('/'))
	{
		include_once("../site_configuration/configuration.php");
		include_once("../lib/user/login/Login.php");
		
		include_once("../lib/location/Location.php");
		
		$location = new Location();//USED TO SILENTLY GRAB LOCATION DATA FROM JAVASCRIPT

		$login = new Login();

		if ($login->isUserLoggedIn() == true)
		{
			header( 'Location: home.php' );
		}

		include('../page/loadHead.php');
		include('../page/body/openBody.php');
		//include('../page/loadHeader.php');

		include("../html/www/frontpage.html");

		include('../page/loadScripts.php');
		include('../page/loadFooter.php');
		include('../page/body/closeBody.php');

	}
	else
	{
		http_response_code(404);
		include('404.php');
		die();
	}

}
?>