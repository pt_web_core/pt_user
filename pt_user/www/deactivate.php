<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/account/Account.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');

$account = new Account();
$account->deactivateAccount();

if (isset($account)) 
{
	if ($account->error) 
	{ foreach ($account->error as $error) { print $error; } }
	if ($account->good) 
	{ foreach ($account->good as $good) { print $good; } }
	if ($account->caution) 
	{ foreach ($account->caution as $caution) { print $caution; } }
}

if ($account->isAccountDeactivated() == true)
{
	header( 'Location: logout.php' ) ;
}

include ('../html/www/deactivate.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

?>