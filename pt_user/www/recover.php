<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");
include_once("../lib/user/account/Account.php");
include_once("../lib/messaging/email/sendEmail.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');

$account = new Account();
$account->requestRecovery();

if (isset($account)) 
{
	if ($account->error) 
	{ foreach ($account->error as $error) { print $error; } }
	if ($account->good) 
	{ foreach ($account->good as $good) { print $good; } }
	if ($account->caution) 
	{ foreach ($account->caution as $caution) { print $caution; } }
}

if ($account->isRecoveryRequested()== true) //Send Recovery Email.
{
	$recovery_link = $_SERVER['SERVER_NAME']."/recover.php?email=".$account->response['email']."&recovery_ticket=".$account->response['recovery_ticket']."&activation_key=".$account->response['activation_key'];
	$email_message = file_get_contents('../html/email/recover_account.html');
	$email_message = str_replace('@RECOVERY_LINK@', $recovery_link, $email_message);
	$email_message = str_replace('@COMPANY_NAME@', COMPANY_NAME, $email_message);

	sendEmail($account->response['email'], "Account recovery", $email_message);
}

if ($account->isAccountRecovered() == true)
{
	header( 'Location: password.php' ) ;
}


include ('../html/www/recover.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

?>