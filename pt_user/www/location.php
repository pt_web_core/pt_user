<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");
include_once("../lib/location/Location.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');

$login = new Login();
$location = new Location();

if ($login->isUserLoggedIn() == true)
{
	if (isset($location)) 
	{
		if ($location->response["error"]) 
		{ foreach ($location->response["error"] as $error) { print $error; } }
		if ($location->response["good"]) 
		{ foreach ($location->response["good"] as $good) { print $good; } }
		if ($location->response["caution"]) 
		{ foreach ($location->response["caution"] as $caution) { print $caution; } }

		if ($location->response['location'])
		{
			print("<table>");
			print("<tr>");
			print("<th>Country Code</th>"); 
			print("<th>Country Name</th>"); 
			print("<th>Region Name</th>"); 
			print("<th>City Name</th>"); 
			print("<th>Latitude</th>"); 
			print("<th>Longitude</th>"); 
			print("</tr>");
			
			
			print("<tr>");
			
			print("<td>" );
			print($location->response["location"]["country_code"]);
			print("</td>" );
			
			print("<td>" );
			print($location->response["location"]["country_name"]);
			print("</td>" );
			
			print("<td>" );
			print($location->response["location"]["region_name"]);
			print("</td>" );


			print("<td>" );
			print($location->response["location"]["city_name"]);
			print("</td>" );
			
			
			print("<td>" );
			print($location->response["location"]["latitude"]);
			print("</td>" );
			
			print("<td>" );
			print($location->response["location"]["longitude"]);
			print("</td>" );

			print("</tr>");
			print("</table>");
		}

	}
}
else
{
	header( 'Location: /' );
}


include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');


?>