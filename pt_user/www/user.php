<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");
include_once("../lib/user/search/UserQuery.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');


//load page Menu
include('../page/navigation/loadMenu.php');

// Load page Wrapper.
include ('../html/page/page/page_head.html');
include ('../html/page/page/page_body.html');


$login = new Login();

if ($login->isUserLoggedIn() == true)
{
    $search = new UserQuery();

    if (isset($search)) {
        if ($search->response['error'])
        {
            foreach ($search->error as $error)
            {
                print $error;
            }
        }
        if ($search->response['good'])
        {
            foreach ($search->good as $good)
            {
                print $good;
            }
        }
        if ($search->response['caution'])
        {
            foreach ($search->caution as $caution)
            {
                print $caution;
            }
        }
    }

    include('../html/www/user/user.html');

    if (isset($search))
    {
		$abs_page = abs($_REQUEST['page']);
        if (isset($search->response) ? $search->response : null)
        {

            foreach ($search->response['search']['user'] as $value)
            {
                $response = file_get_contents('../html/www/user/user_response.html');
                $response = str_replace('@PICTURE@', '<img src="http://'.$_SERVER[HTTP_HOST].'/demo/img.png">', $response);
//              $response = str_replace('@ID@', $value['id'], $response);
                $response = str_replace('@USERNAME@', '<a href="/message.php?to='.$value['id'].'">'.$value['username'].'</a>', $response);
                $response = str_replace('@FIRSTNAME@', $value['firstname'], $response);
                $response = str_replace('@LASTNAME@', $value['lastname'], $response);
                $response = str_replace('@SEX@', str_replace("_"," ",$value['sex']), $response);
                $response = str_replace('@AGE@', $value['age'], $response);
                print $response;
            }
			
			/******************************************************************************************************************************/
			//PAGE-BOX HERE
			$number_of_pages = $search->getNumberOfPages();
			if($number_of_pages > 0)
			{
				print ('<pager-box>');
				
				if($number_of_pages > 10)
				{
					$i =  $abs_page - 1;
					$to = $abs_page + 9;
				}
				else
				{
					$i = 1;
					$to = 9;
				}
				
				if($_REQUEST['page'] == '' || $abs_page == '1')
				{
					$print_back = false;
				}
				else
				{
					$print_back = true;
				}
				
				
				for ($x = $i; $x <= $to; $x++)
				{
					if ($x == 0)
					{
						continue;
					}
					else if($x > $number_of_pages)
					{
						break;
					}
					else if($print_back)
					{
						$print_back = false;
						print('<a href="user.php? email='.$_GET['email'].'&firstname='.$_GET['firstname'].'&lastname='.$_GET['lastname'].'&username='.$_GET['username'].'&sex='.$_GET['sex'].'&age_from='.$_GET['age_from'].'&age_to='.$_GET['age_to'].'&search_strict='.$_GET['search_strict'].'&page='.($x).'&search_user="> << </a>');
					}
					else if($x == -1)
					{
						print('1, ');
					}
					else if($abs_page == $x)
					{
						if($x == 0 || $x == 1)
						{
							print('1, ');
						}
						else if($x == $number_of_pages)
						{
							print($x);
						}
						else 
						{
							print($x.', ');									
						}
					}
					else if( ($x == $to) && ($x <= $number_of_pages) )
					{
						print('<a href="user.php? email='.$_GET['email'].'&firstname='.$_GET['firstname'].'&lastname='.$_GET['lastname'].'&username='.$_GET['username'].'&sex='.$_GET['sex'].'&age_from='.$_GET['age_from'].'&age_to='.$_GET['age_to'].'&search_strict='.$_GET['search_strict'].'&page='.$x.'&search_user="> >> </a>');
					}
					else if($x == $number_of_pages)
					{
						print('<a href="user.php? email='.$_GET['email'].'&firstname='.$_GET['firstname'].'&lastname='.$_GET['lastname'].'&username='.$_GET['username'].'&sex='.$_GET['sex'].'&age_from='.$_GET['age_from'].'&age_to='.$_GET['age_to'].'&search_strict='.$_GET['search_strict'].'&page='.$x.'&search_user="> '.$x.'  </a>');
					}
					else if($x != 1)
					{
						print('<a href="user.php? email='.$_GET['email'].'&firstname='.$_GET['firstname'].'&lastname='.$_GET['lastname'].'&username='.$_GET['username'].'&sex='.$_GET['sex'].'&age_from='.$_GET['age_from'].'&age_to='.$_GET['age_to'].'&search_strict='.$_GET['search_strict'].'&page='.$x.'&search_user="> '.$x.',  </a>');
					}
					else
					{
						//DO NOTHING!!!
						continue;
					}
				}
				
				print ('</pager-box>');
			}
			/******************************************************************************************************************************/
        }
    }
}

else
{
    header( 'Location: index.php' );
}

include ('../html/page/page/page_foot.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

/******************************************************************************************************************************/
?>