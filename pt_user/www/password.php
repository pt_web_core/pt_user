<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");
include_once("../lib/user/account/Account.php");
include_once("../lib/user/signup/Signup.php");
include_once("../lib/user/activate/ActivationKey.php");
include_once("../lib/user/password/Password.php");
include_once("../lib/messaging/email/sendEmail.php");
	
include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');

//load page Menu
include('../page/navigation/loadMenu.php');

// Load page Wrapper.
include ('../html/page/page/page_head.html');
include ('../html/page/page/page_body.html');


$login = new Login();
$account = new Account();

$signup = new Signup();
$signup ->checkSignupID();

$password = new Password();
$password->createNewPassword();

$activation_key = new ActivationKey();
$activation_key->createActivationKey();
$activation_key->createNewActivationKey();
$activation_key->checkActivationKey();


if ( ($login->isUserLoggedIn() != true) && ($account->isAccountRecovered() != true) && ($signup->isUserSignedUp() == false) )
{
	header( 'Location: signup.php' );
}

if (isset($password)) 
{
	if ($password->response['error']) 
	{ foreach ($password->response['error'] as $error) { print $error; } }
	if ($password->response['good']) 
	{ foreach ($password->response['good'] as $good) { print $good; } }
	if ($password->response['caution']) 
	{ foreach ($password->response['caution'] as $caution) { print $caution; } }
}

if ( ($password->isPasswordSet() == true) && ($signup->isUserSignedUp() == true) ) //Send Activation Email.
{
	$activation_link = $_SERVER['SERVER_NAME']."/activate.php?email=".$_SESSION['email']."&activation_key=".$_SESSION['activation_key'];
	$email_message = file_get_contents('../html/email/activate_account.html');
	$email_message = str_replace('@ACTIVATION_LINK@', $activation_link, $email_message);
	$email_message = str_replace('@COMPANY_NAME@', COMPANY_NAME, $email_message);
	
	sendEmail($_SESSION['email'], "Account activation", $email_message);

}

if ( ($password->isPasswordSet() == true) && ($account->isAccountRecovered() == true) )
{
	header( 'Location: logout.php' ) ;
}

if ($password->isPasswordSet() == true)
{
	header( 'Location: index.php' ) ;
}

include ('../html/www/password.html');


include ('../html/page/page/page_foot.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

?>