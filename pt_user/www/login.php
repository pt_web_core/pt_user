<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');

$login = new Login();
if (isset($login)) 
{
	if ($login->response['error'])
	{ foreach ($login->response['error'] as $error) { print $error; } }
	if ($login->response['good'])
	{ foreach ($login->response['good'] as $good) { print $good; } }
	if ($login->response['caution'])
	{ foreach ($login->response['caution'] as $caution) { print $caution; } }
}

if ($login->isUserLoggedIn() == true)
{
    header( 'Location: user.php' ) ;
}

include ('../html/www/login.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

?>