<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");
include_once("../lib/user/signup/Signup.php");
include_once("../lib/user/account/Account.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');

$login = new Login();
$signup = new Signup();
$account = new Account();
$account->activateAccount();


if ($login->isUserLoggedIn() == true)
{
	header( 'Location: index.php' );
}
if ($account->isAccountActivated() == true)
{
	header( 'Location: index.php' );
}
if (isset($account)) 
{
	if ($account->error) 
	{ foreach ($account->error as $error) { print $error; } }
	if ($account->good) 
	{ foreach ($account->good as $good) { print $good; } }
	if ($account->caution) 
	{ foreach ($account->caution as $caution) { print $caution; } }
}

include ('../html/www/activate.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

//"http://$_SERVER[HTTP_HOST]/activate.php?email=$email&activation_key=$activation_key"

?>