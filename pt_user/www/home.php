<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');

$login = new Login();
if ($login->isUserLoggedIn() == false)
{
	header( 'Location: index.php' );
}

include ('../html/www/home.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

?>