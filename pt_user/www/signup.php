<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");
include_once("../lib/user/signup/Signup.php");
include_once("../lib/user/activate/ActivationKey.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');

$login = new Login();
$signup = new Signup();
$signup ->checkSignupID();

$activation_key = new ActivationKey();
$activation_key ->createActivationKey();

if ($login->isUserLoggedIn() == true)
{
	header( 'Location: index.php' );
}

if (isset($signup)) 
{
    if ($signup->response['error'])
    { foreach ($signup->response['error'] as $error) { print $error; } }
    if ($signup->response['good'])
    { foreach ($signup->response['good'] as $good) { print $good; } }
    if ($signup->response['caution'])
    { foreach ($signup->response['caution'] as $caution) { print $caution; } }
}

if ($signup->isUserSignedUp() == true)
{
	header( 'Location: password.php' ) ;
}

include ('../html/www/signup.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

?>