<?php

include_once("../site_configuration/configuration.php");
include_once("../lib/user/login/Login.php");
include_once("../lib/device/Device.php");

include('../page/loadHead.php');
include('../page/body/openBody.php');
include('../page/loadHeader.php');


//load page Menu
include('../page/navigation/loadMenu.php');

// Load page Wrapper.
include ('../html/page/page/page_head.html');
include ('../html/page/page/page_body.html');


$login = new Login();

if ($login->isUserLoggedIn() == true)
{
    $device = new Device();

    if (isset($device)) {
        if ($device->response['error'])
        {
            foreach ($device->error as $error)
            {
                print $error;
            }
        }
        if ($device->response['good'])
        {
            foreach ($device->good as $good)
            {
                print $good;
            }
        }
        if ($device->response['caution'])
        {
            foreach ($device->caution as $caution)
            {
                print $caution;
            }
        }
    }
	if($device->hasDevices())
	{
		include('../html/www/device/device.html');		
	}
	else
	{
		include('../html/www/device/device_empty.html');		
	}

	$output_index = 0;
    if (isset($device))
    {
		if(isset($_REQUEST['page']))
		{
			$abs_page = abs($_REQUEST['page']);	
		}
		else
		{
			$abs_page = 1;	
		}


		
        if (isset($device->response) ? $device->response : null)
        {
            foreach ($device->response['device'] as $value)
            {
				if($output_index > 1)
				{
					$response = file_get_contents('../html/www/device/device_response.html');
					$response = str_replace('@DEVICE_ID@', $value['device_id'], $response);
					$response = str_replace('@DEVICE_NAME@', $value['device_name'], $response);
					print $response;
				}
				$output_index++;
            }
			
			/******************************************************************************************************************************/
			//PAGE-BOX HERE
			$number_of_pages = $device->getNumberOfPages();
			if($number_of_pages > 0)
			{
				print ('<pager-box>');
				
				if($number_of_pages > 10)
				{
					$i =  $abs_page - 1;
					$to = $abs_page + 9;
				}
				else
				{
					$i = 1;
					$to = 9;
				}
				
				if($_REQUEST['page'] == '' || $abs_page == '1')
				{
					$print_back = false;
				}
				else
				{
					$print_back = true;
				}
				
				
				for ($x = $i; $x <= $to; $x++)
				{
					if ($x == 0)
					{
						continue;
					}
					else if($x > $number_of_pages)
					{
						break;
					}
					else if($print_back)
					{
						$print_back = false;
						print('<a href="device.php? remove_device='.$_GET['remove_device'].'&page='.($x).'&device_user="> << </a>');
					}
					else if($x == -1)
					{
						print('1, ');
					}
					else if($abs_page == $x)
					{
						if($x == 0 || $x == 1)
						{
							print('1, ');
						}
						else if($x == $number_of_pages)
						{
							print($x);
						}
						else 
						{
							print($x.', ');									
						}
					}
					else if( ($x == $to) && ($x <= $number_of_pages) )
					{
						print('<a href="device.php? remove_device='.$_GET['remove_device'].'&page='.$x.'&device_user="> >> </a>');
					}
					else if($x == $number_of_pages)
					{
						print('<a href="device.php? remove_device='.$_GET['remove_device'].'&page='.$x.'&device_user="> '.$x.'  </a>');
					}
					else if($x != 1)
					{
						print('<a href="device.php? remove_device='.$_GET['remove_device'].'&page='.$x.'&device_user="> '.$x.',  </a>');
					}
					else
					{
						//DO NOTHING!!!
						continue;
					}
				}
				
				print ('</pager-box>');
			}
			/******************************************************************************************************************************/
        }
    }
}

else
{
    header( 'Location: index.php' );
}

include ('../html/page/page/page_foot.html');

include('../page/loadScripts.php');
include('../page/loadFooter.php');
include('../page/body/closeBody.php');

/******************************************************************************************************************************/
?>