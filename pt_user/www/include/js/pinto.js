/*!

 *	PintoJS 
 *	Copyright 2013 Promyk Zamojski
 *	Released under the Apache License Version 2.0
 *  January 2004 http://www.apache.org/licenses/

*/

if (window.location.protocol == "file:") { var URL = '..'; }
else { var URL = window.location.protocol + "//" + window.location.hostname + "/include"; }

function getFavicon(){
document.write('<link rel="icon" type="image/svg+xml" href="'+URL+'/icon/favicon.svg">');
document.write('<link rel="alternate icon" type="image/png" href="'+URL+'/icon/favicon.png">');
}
 //Import Favicon.
getFavicon(); 

function getModernUITile(){
if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){
document.write('<meta name="application-name" content="Application"/>');
document.write('<meta name="msapplication-tooltip" content="Keep up-to-date"/>');
document.write('<meta name="msapplication-TileImage" content="'+URL+'/icon/modern-ui-tile.png"/>');
document.write('<meta name="msapplication-TileColor" content="#2a7fff"/>');
}}
 //Import Modern UI Tile.
getModernUITile(); 

function getiOSIcon(){
if (/iPhone|iPad|iPod/.test(navigator.userAgent)){
document.write('<link rel="apple-touch-icon" sizes="57x57" href="'+URL+'/icon/iPhone-NonRetina.png" />');
document.write('<link rel="apple-touch-icon" sizes="72x72" href="'+URL+'/icon/iPad-NonRetina.png" />');
document.write('<link rel="apple-touch-icon" sizes="114x114" href="'+URL+'/icon/iPhone-Retina.png" />');
document.write('<link rel="apple-touch-icon" sizes="144x144" href="'+URL+'/icon/iPad-Retina.png" />');
}}
 //Import iOS Icon.
getiOSIcon(); 