var url = window.location.pathname;
var page = url.replace(".php", ""); 
var name = page.replace("/", ""); 

if (window.location.protocol == "file:") { var URL = '..'; }
else { var URL = window.location.protocol + "//" + window.location.hostname + "/include"; }

function loadPageResource()
{
	if ((window.innerWidth > 1025) && name != "" && name != "index")
		{
			document.write('<script src="'+URL+'/js/page/'+name+'.js" type="text/javascript"></script>');
			document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/page/'+name+'.css" media="screen" />');
		}
	if ((window.innerWidth < 1025) && name != "" && name != "index")
		{
			document.write('<script src="'+URL+'/js/page/'+name+'.js" type="text/javascript"></script>');
            document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/page/'+name+'.css" media="screen" />');
			document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/page/'+name+'_mobile.css" media="screen" />');
		}
	if ((window.innerWidth > 1025) && name == "" || name == "index")
		{
			document.write('<script src="'+URL+'/js/page/frontpage.js" type="text/javascript"></script>');
			document.write('<link rel="stylesheet" type="text/css"  href="'+URL+'/css/page/frontpage.css" media="screen">');
		}
	if ((window.innerWidth < 1025) && name == "" || name == "index")
		{
			document.write('<script src="'+URL+'/js/page/frontpage.js" type="text/javascript"></script>');
            document.write('<link rel="stylesheet" type="text/css"  href="'+URL+'/css/page/frontpage.css" media="screen">');
			document.write('<link rel="stylesheet" type="text/css"  href="'+URL+'/css/page/frontpage_mobile.css" media="screen">');
		}
}

function setScreenDimensionsToCookie()
{
	document.cookie = "screen_width="+window.innerWidth;
	document.cookie = "screen_height="+window.innerHeight;
}


 //Writes CSS and JavaScript Library.
loadPageResource();

// Sets screen size to cookie for server side use.
setScreenDimensionsToCookie();
