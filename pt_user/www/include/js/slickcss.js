/*!

 *	SlickCSS 
 *	Copyright 2012 Promyk Zamojski
 *	Released under the Apache License Version 2.0
 *  January 2004 http://www.apache.org/licenses/

*/

if (window.location.protocol == "file:") { var URL = '..'; }
else { var URL = window.location.protocol + "//" + window.location.hostname + "/include"; }

function GetMobileSlickcss(){

 if ((window.innerWidth < 1024) && (!window.cordova)){
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/slick_mobile.css" media="screen" />');
}}

//Writes mobile browser css file to webpage.
GetMobileSlickcss();