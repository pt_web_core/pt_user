function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(sendPosition);
  }
}

function sendPosition(position) {

	var http = new XMLHttpRequest();
	var url = '/';
	var params = 'latitude=' + position.coords.latitude + '&longitude=' + position.coords.longitude;
	http.open('POST', url, true);

	//Send the proper header information along with the request
	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

	http.send(params);
  
}

//GET UPDATED LOCATION
getLocation();