/*!

 *	Wrap.css 
 *	Copyright 2013 Promyk Zamojski
 *	Released under the Apache License Version 2.0
 *  January 2004 http://www.apache.org/licenses/

*/

if (window.location.protocol == "file:") { var URL = '..'; }
else { var URL = window.location.protocol + "//" + window.location.hostname + "/include"; }

function GetMobileWrapCss(){

 if (window.innerWidth < 1024){
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/wrap_mobile.css" media="screen" />');
}}

//Writes mobile browser css file to webpage.
GetMobileWrapCss();