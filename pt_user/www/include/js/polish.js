/*!
 *	PolishCSS 
 *	Copyright 2013 Promyk Zamojski
 *	Released under the Apache License Version 2.0
 *  January 2004 http://www.apache.org/licenses/
*/

// Load web browser layout engine specific css file.
// Cascading Style Sheets are rendered differently by each browser layout engine.
// This is a work around to correct the inconsistency amongst web browsers.

if (window.location.protocol == "file:") { var URL = '..'; }
else { var URL = window.location.protocol + "//" + window.location.hostname + "/include"; }

function getBrowserPolishCss()
{
	if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_gecko.css" media="screen" />');
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_legacy_input.css" media="screen" />');
	}

	else 
	{
	
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_trident.css" media="screen" />');
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_legacy_input.css" media="screen" />');
	} 
	
	else 
	{
	if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent))
	{
    document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_presto.css" media="screen" />');
	} 
	
	else 
	{
    if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent))
	{
     document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_blink.css" media="screen" />');
	} 
	
	else 
	{
	
	if ((!/Android/i.test (navigator.userAgent) &&(/WebKit[\/\s](\d+\.\d+)/.test(navigator.userAgent))))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_webkit.css" media="screen" />')
	} 
	
	else 
	{	 
	if ((/Android/i.test (navigator.userAgent) && (/WebKit[\/\s](\d+\.\d+)/.test(navigator.userAgent))))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_android_webkit.css" media="screen" />')
	}
	 
	}
	}
	}
	}
	}
}

//Writes web browser layout engine specific css to webpage.
getBrowserPolishCss(); 


 

 function getMobilePolishCss()
{
// Load mobile browser css file.
if ((window.innerWidth < 1025) && (!window.cordova))
{
// Mobile Polish.
document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_mobile.css" media="screen" />');
//Writes mobile browser specific css placeholder to webpage.
 document.write ('<link id="mobile_style" rel="stylesheet" type="text/css" href="no_mobile_style" media="screen" />');
}


	// Load mobile browser specific css file. 	
	if ((window.innerWidth < 1025) && /Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_gecko_mobile.css" />');
	} 
	
	else 
	{
 	if ((window.innerWidth < 1025) && /MSIE[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_trident_mobile.css" />');
	} 
	
	else 
	{
	if ((window.innerWidth < 1025) && /Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_blink_mobile.css" />');
	} 
	
	else 
	{
	if ((window.innerWidth < 1025) && /Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_blink_mobile.css" />');
	} 
	
	else 
	{
	if ((window.innerWidth < 1025) && /WebKit[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova))
	{
	document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish_webkit_mobile.css" />');
	} 
	
	else 
	{
	}
	
	}
	}
	}
	}
}

//Writes mobile browser specific css file to webpage.
getMobilePolishCss();