/*!

 *	Fabric 
 *	Copyright 2013 Promyk Zamojski
 *	Released under the Apache License Version 2.0
 *  January 2004 http://www.apache.org/licenses/

*/

if (window.location.protocol == "file:") { var URL = '..'; }
else { var URL = window.location.protocol + "//" + window.location.hostname + "/include"; }

function getCssLibrary(){
document.write('<script src="'+URL+'/js/pinto.js" type="text/javascript"></script>');
document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/colure.css" media="screen" />');
document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/slick.css" media="screen" />');
document.write('<script type="text/javascript" src="'+URL+'/js/slickcss.js"></script>');
document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/polish.css" media="screen" />');
document.write('<script type="text/javascript" src="'+URL+'/js/polish.js"></script>');
document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/searchbox.css" media="screen" />');
document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/pager.css" media="screen" />');
document.write('<script type="text/javascript" src="'+URL+'/js/searchbox.js"></script>');
document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/wrap.css" media="screen" />');
document.write('<script src="'+URL+'/js/wrap.js" type="text/javascript"></script>');
document.write('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui.css" media="screen" />');
}
 //Writes Css and JavaScript Library.
getCssLibrary(); 



 function getUICss(){

 if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
    document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_gecko.css" media="screen" />');} else {

  if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){
   document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_trident.css" media="screen" />');} else {

   if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
    document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_blink.css" media="screen" />');} else {

    if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
     document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_blink.css" media="screen" />');} else {
	
     if (/WebKit[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
     document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_webkit.css" media="screen" />');}	 
	 
 }}}}}

 //Writes web browser layout engine specific css to webpage.
getUICss(); 

 
 function getMobileUICss(){
 
 // Load mobile browser css file.
 if ((window.innerWidth < 1025) && (!window.cordova)){

 // Mobile UI.
 document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_mobile.css" media="screen" />');

 }


 // Get mobile browser specific css file information.
 // Load mobile browser specific css file. 

 if ((window.innerWidth < 1025) && /Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova)){
    document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_gecko_mobile.css" media="screen" />');} else {
 
  if ((window.innerWidth < 1025) && /MSIE[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova)){
     document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_trident_mobile.css" media="screen" />');} else {

   if ((window.innerWidth < 1025) && /Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova)){
      document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_blink_mobile.css" media="screen" />');} else {

    if ((window.innerWidth < 1025) && /Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova)){
       document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_blink_mobile.css" media="screen" />');} else {

     if ((window.innerWidth < 1025) && /WebKit[\/\s](\d+\.\d+)/.test(navigator.userAgent) && (!window.cordova)){
        document.write ('<link rel="stylesheet" type="text/css" href="'+URL+'/css/ui_webkit_mobile.css" media="screen" />');} else {
 
 //Writes mobile browser specific css placeholder to webpage.
 document.write ('<link id="ui_mobile_style" rel="stylesheet" type="text/css" href="no_ui_mobile_style" media="screen" />');
 
}}}}}}

 //Writes mobile browser specific css file to webpage.
getMobileUICss();