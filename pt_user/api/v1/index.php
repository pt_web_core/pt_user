<?php

include_once('../../site_configuration/configuration.php');

//URL
include_once("../../lib/url/URLPath.php");

//LOGIN
include_once("../../lib/user/login/RESTAPILogin.php");

//input json
$json_decoded_array = array();

header('Content-type: application/json'); 
$json_decoded_array = json_decode(file_get_contents('php://input'), true);			//JSON HTTP POST Decode.


$url_path = new URLPath();

if($url_path->get('/v1/loopback'))
{
	print(json_encode($json_decoded_array));
}

//else if($url_path->assign('/v1/404', '404.php'));

else
{
	RESTAPIDeviceAccessTokenAuthorize::getDeviceSession($json_decoded_array);
	if(RESTAPILogin::isRESTAPIUserLoggedIn())
	{
		/*
		//input json

		if ($login->isUserLoggedIn() == true)
		{	
			try
			{
				$rest = new RESTtoSQL();
				print(json_encode(array_merge($login->response, $device->response, $rest->response)));
			}
			catch(Exception $e)
			{
				print $e;
			}
		}
		*/	
	}

}

?>