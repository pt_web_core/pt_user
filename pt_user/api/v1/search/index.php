<?php

include_once('../../../site_configuration/configuration.php');

//LOGIN
include_once("../../../lib/user/login/RESTAPILogin.php");

//SEARCH
include_once("../../../lib/user/search/RESTAPIUserQuery.php");
include_once("../../../lib/user/search/RESTAPIUserSearch.php");
include_once("../../../lib/device/RESTAPIDeviceAccessTokenAuthorize.php");

//input json
$json_decoded_array = array();

header('Content-type: application/json');
$json_decoded_array = json_decode(file_get_contents('php://input'), true);			//JSON HTTP POST Decode.



/**********************************************************************************************************/
print("{" );
/**********************************************************************************************************/
RESTAPIDeviceAccessTokenAuthorize::getDeviceSession($json_decoded_array);
if(RESTAPILogin::isRESTAPIUserLoggedIn())
{
	$user_search = new RESTAPIUserSearch($json_decoded_array);

	if( isset($user_search->response) )
	{
		if ($user_search->response['error'])
		{
			print("error:" ); 
			print(json_encode( $user_search->response['error'] ) ); 
		}
		if ($user_search->response['good'])
		{
			print("good:" ); 
			print(json_encode( $user_search->response['good'] ) );
		}
		if ($user_search->response['caution'])
		{
			print("caution:" ); 
			print(json_encode( $user_search->response['caution'] ) );
		}
		if ($user_search->response['search'])
		{
			print("search:" ); 
			print(json_encode( $user_search->response['search'] ) );
		}
	}
}

/**********************************************************************************************************/
print("}" );
/**********************************************************************************************************/

?>