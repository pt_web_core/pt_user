<?php

include_once('../../../../site_configuration/configuration.php');


//LOGIN
include_once("../../../../lib/user/login/RESTAPILogin.php");

//DEVICE
include_once("../../../../lib/device/RESTAPIDeviceAccessTokenAuthorize.php");

//SEARCH
include_once("../../../../lib/user/search/RESTAPIUserQuery.php");
include_once("../../../../lib/user/search/RESTAPIUserSearch.php");

//input json
$json_decoded_array = array();

header('Content-type: application/json');
$json_decoded_array = json_decode(file_get_contents('php://input'), true);			//JSON HTTP POST Decode.
//input json


/**********************************************************************************************************/
print("{" );
/**********************************************************************************************************/
RESTAPIDeviceAccessTokenAuthorize::getDeviceSession($json_decoded_array);
if(RESTAPILogin::isRESTAPIUserLoggedIn())
{
	$user_query = new RESTAPIUserQuery($json_decoded_array);

	if( isset($user_query->response) )
	{
		if ($user_query->response['error'])
		{
			print("error:" ); 
			print(json_encode( $user_query->response['error'] ) ); 
		}
		if ($user_query->response['good'])
		{
			print("good:" ); 
			print(json_encode( $user_query->response['good'] ) );
		}
		if ($user_query->response['caution'])
		{
			print("caution:" ); 
			print(json_encode( $user_query->response['caution'] ) );
		}
		if ($user_query->response['search'])
		{
			print("search:" ); 
			print(json_encode( $user_query->response['search'] ) );
		}
	}
}

/**********************************************************************************************************/
print("}" );
/**********************************************************************************************************/

?>