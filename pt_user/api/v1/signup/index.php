<?php

include_once('../../../site_configuration/configuration.php');

//SIGNUP
include_once("../../../lib/user/signup/RESTAPISignup.php");
include_once("../../../lib/device/RESTAPIDeviceAccessTokenSignup.php");

//input json
$json_decoded_array = array();

header('Content-type: application/json'); 
$json_decoded_array = json_decode(file_get_contents('php://input'), true);			//JSON HTTP POST Decode.

/**********************************************************************************************************/
print("{" );
/**********************************************************************************************************/

$signup = new RESTAPISignup($json_decoded_array);

if( isset($signup->response) )
{
	if ($signup->response['error'])
	{
		print("error:" ); 
		print(json_encode( $signup->response['error'] ) ); 
	}
	if ($signup->response['good'])
	{
		print("good:" ); 
		print(json_encode( $signup->response['good'] ) );
	}
	if ($signup->response['caution'])
	{
		print("caution:" ); 
		print(json_encode( $signup->response['caution'] ) );
	}
	if ($signup->response['user'])
	{
		print("user:" ); 
		print(json_encode( $signup->response['user'] ) );
	}
}

/**********************************************************************************************************/

$device_access_token_signup = new RESTAPIDeviceAccessTokenSignup($json_decoded_array);//FIX ME

if(isset($device_access_token_signup->response) && isset($signup->response))
{
	print(",");
}

if( isset($device_access_token_signup->response) )
{
	if ($device_access_token_signup->response['error'])
	{
		print("error:" ); 
		print(json_encode( $device_access_token_signup->response['error'] ) ); 
	}
	if ($device_access_token_signup->response['good'])
	{
		print("good:" ); 
		print(json_encode( $device_access_token_signup->response['good'] ) );
	}
	if ($device_access_token_signup->response['caution'])
	{
		print("caution:" ); 
		print(json_encode( $device_access_token_signup->response['caution'] ) );
	}
	if ($device_access_token_signup->response['device'])
	{
		print("device:" ); 
		print(json_encode( $device_access_token_signup->response['device'] ) );
	}
}

/**********************************************************************************************************/
print("}" );
/**********************************************************************************************************/

?>