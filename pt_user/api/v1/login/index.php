<?php

include_once('../../../site_configuration/configuration.php');

//LOGIN
include_once("../../../lib/user/login/RESTAPILogin.php");
include_once("../../../lib/device/RESTAPIDeviceAccessTokenRequest.php");
include_once("../../../lib/device/RESTAPIDeviceAccessTokenAuthorize.php");


//input json
$json_decoded_array = array();

header('Content-type: application/json'); 
$json_decoded_array = json_decode(file_get_contents('php://input'), true);			//JSON HTTP POST Decode.

/**********************************************************************************************************/
print("{" );
/**********************************************************************************************************/
$login = new RESTAPILogin($json_decoded_array);


if (isset($login->response)) 
{
	if ($login->response['error'])
	{
		print("error:" ); 
		print(json_encode( $login->response['error'] ) ); 
	}
	if ($login->response['good'])
	{
		print("good:" ); 
		print(json_encode( $login->response['good'] ) );
	}
	if ($login->response['caution'])
	{
		print("caution:" ); 
		print(json_encode( $login->response['caution'] ) );
	}
	if ($login->response['user'])
	{
		print("user:" ); 
		print(json_encode( $login->response['user'] ) );
	}
}

/**********************************************************************************************************/
$device_access_token_request = new RESTAPIDeviceAccessTokenRequest($json_decoded_array);

if (isset($device_access_token_request->response)) 
{

	if ($device_access_token_request->response['error'])
	{
		print("error:" ); 
		print(json_encode( $device_access_token_request->response['error'] ) ); 
	}
	if ($device_access_token_request->response['good'])
	{
		print("good:" ); 
		print(json_encode( $device_access_token_request->response['good'] ) );
	}
	if ($device_access_token_request->response['caution'])
	{
		print("caution:" ); 
		print(json_encode( $device_access_token_request->response['caution'] ) );
	}
	if ($device_access_token_request->response['device'])
	{
		print("device:" ); 
		print(json_encode( $device_access_token_request->response['device'] ) );
	}
}

/**********************************************************************************************************/

$device_access_token_authorize = new RESTAPIDeviceAccessTokenAuthorize($json_decoded_array);
if (isset($device_access_token_authorize->response)) 
{

	if ($device_access_token_authorize->response['error'])
	{
		print("error:" ); 
		print(json_encode( $device_access_token_authorize->response['error'] ) ); 
	}
	if ($device_access_token_authorize->response['good'])
	{
		print("good:" ); 
		print(json_encode( $device_access_token_authorize->response['good'] ) );
	}
	if ($device_access_token_authorize->response['caution'])
	{
		print("caution:" ); 
		print(json_encode( $device_access_token_authorize->response['caution'] ) );
	}
	if ($device_access_token_authorize->response['device'])
	{
		print("device:" ); 
		print(json_encode( $device_access_token_authorize->response['device'] ) );
	}
}

/**********************************************************************************************************/
print("}" );
/**********************************************************************************************************/


/*
session_id($json_decoded_array["device"]["access_token"]);
print_r($_SESSION);
*/
?>