<?php

include_once('../../../site_configuration/configuration.php');

//LOGIN
include_once("../../../lib/user/login/RESTAPILogin.php");

//DEVICE
include_once("../../../lib/device/RESTAPIDevice.php");
include_once("../../../lib/device/RESTAPIDeviceAccessTokenAuthorize.php");

//input json
$json_decoded_array = array();

header('Content-type: application/json');
$json_decoded_array = json_decode(file_get_contents('php://input'), true);			//JSON HTTP POST Decode.



/**********************************************************************************************************/
print("{" );
/**********************************************************************************************************/
RESTAPIDeviceAccessTokenAuthorize::getDeviceSession($json_decoded_array);
if(RESTAPILogin::isRESTAPIUserLoggedIn())
{
	$device = new RESTAPIDevice($json_decoded_array);

	if( isset($device->response) )
	{
		if ($device->response['error'])
		{
			print("error:" ); 
			print(json_encode( $device->response['error'] ) ); 
		}
		if ($device->response['good'])
		{
			print("good:" ); 
			print(json_encode( $device->response['good'] ) );
		}
		if ($device->response['caution'])
		{
			print("caution:" ); 
			print(json_encode( $device->response['caution'] ) );
		}
		if ($device->response['device'])
		{
			print("device:" ); 
			print(json_encode( $device->response['device'] ) );
		}
	}
}

/**********************************************************************************************************/
print("}" );
/**********************************************************************************************************/

?>