<?php

include_once('../../../site_configuration/configuration.php');

//LOGIN
include_once("../../../lib/user/login/RESTAPILogin.php");

//DEVICE
include_once("../../../lib/device/RESTAPIDeviceAccessTokenAuthorize.php");

//LOCATION
include_once("../../../lib/location/RESTAPILocation.php");

//input json
$json_decoded_array = array();

header('Content-type: application/json');
$json_decoded_array = json_decode(file_get_contents('php://input'), true);			//JSON HTTP POST Decode.



/**********************************************************************************************************/
print("{" );
/**********************************************************************************************************/
RESTAPIDeviceAccessTokenAuthorize::getDeviceSession($json_decoded_array);
if(RESTAPILogin::isRESTAPIUserLoggedIn())
{
	$location = new RESTAPILocation($json_decoded_array);

	if( isset($location->response) )
	{
		if ($location->response['error'])
		{
			print("error:" ); 
			print(json_encode( $location->response['error'] ) ); 
		}
		if ($location->response['good'])
		{
			print("good:" ); 
			print(json_encode( $location->response['good'] ) );
		}
		if ($location->response['caution'])
		{
			print("caution:" ); 
			print(json_encode( $location->response['caution'] ) );
		}
		if ($location->response['location'])
		{
			print("location:" ); 
			print(json_encode( $location->response['location'] ) );
		}
	}
}

/**********************************************************************************************************/
print("}" );
/**********************************************************************************************************/

?>