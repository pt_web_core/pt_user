{
"error": {
 "code": 404,
 "message": "Either there is no API method associated with the URL path of the request, or the request refers to one or more resources that were not found."
 }
}