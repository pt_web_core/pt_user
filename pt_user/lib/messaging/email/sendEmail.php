<?php
function sendEmail($email_to, $email_subject, $email_message)
{
	$headers   = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/html; charset=UTF-8";
	$headers[] = "From: ".COMPANY_NAME." <".DOMAIN_MAILER.">";
	$headers[] = "Subject: {$email_subject}";
	$headers[] = "X-Mailer:".DOMAIN_NAME;

	mail($email_to, $email_subject, $email_message, implode("\r\n", $headers));
}
?>