<?php
//TODO WIP
class RESTtoSQL
{

	private $db_connection;
	private $db_structure = array();
	private $uri_segments = array();
	/*
	 * Array of custom table indexes
	 */
	private $table_index = array();
	
	
	public $response = array();
	
	public function __construct()
	{
		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if (!$this->db_connection->set_charset("utf8"))
		{
			// Repurpose unused error code to check utf8 character set in database
			$this->response["error"] = array(
				'message' => " I'm a teapot (RFC 2324), Looks like a Database connection problem.",
				'code' => 418
			);
		}

		$this->db_structure = $this->mapDatabase(DB_NAME);
		$this->uri_segments = $this->getURISegments('');

		$this->getRESTAPI();
	}

	/*
	 * Handle the REST calls and map them to corresponding CRUD
	 */
	public function getRESTAPI()
	{
		header('Content-type: application/json');
		/*
		CREATE()	> POST		/table
		READ()		> GET		/table[/id]
		UPDATE()	> PUT		/table/id
		DELETE()	> DELETE	/table/id
		*/
		switch ($_SERVER['REQUEST_METHOD'])
		{
			case 'POST':
				$this->CREATE();
				break;
			case 'GET':
				$this->READ();
				break;
			case 'PUT':
				$this->UPDATE();
				break;
			case 'DELETE':
				$this->DELETE();
				break;
		}
	}

	/*
	 * Add a custom index (usually primary key) for a table
	 */
	public function setTableIndex($table_name, $index_field)
	{
		$this->table_index[$table_name] = $index_field;
	}

	/*
	 * Map the stucture of the MySQL db to an array
	 */
	private function mapDatabase($database_name)
	{
		// Map db structure to array
		$tables_array = array();

		$sql = "SHOW TABLES FROM ".$database_name;

		while($database_name == $this->db_connection->query($sql)->fetch_array())
		{
			if(isset( $table['Tables_in_'.$database_name] )){
				$table_name = $table['Tables_in_'. $database_name];
				$tables_array[$table_name] = array();
			}
		}
		foreach($tables_array as $table_name=>$val)
		{
			$sql = "SHOW TABLES FROM ".$table_name;

			$fields = $this->db_connection->query($sql)->fetch_all();
			$tables_array[$table_name] = $fields;
		}
		return $tables_array;
	}

	/*
	 * Get the URI segments from the URL
	 */
	private function getURISegments($base_uri)
	{
		// Fix REQUEST_URI if required
		if(!isset($_SERVER['REQUEST_URI'])){
			$_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'], 1);
			if(isset($_SERVER['QUERY_STRING'])) $_SERVER['REQUEST_URI'] .= '?'. $_SERVER['QUERY_STRING'];
		}

		$url = '';
		$request_url = $_SERVER['REQUEST_URI'];
		$script_url  = $_SERVER['PHP_SELF'];
		$request_url = str_replace($base_uri, '', $request_url);
		if($request_url != $script_url) $url = trim(preg_replace('/'. str_replace('/', '\/', str_replace('index.php', '', $script_url)) .'/', '', $request_url, 1), '/');
		$url = rtrim(preg_replace('/\?.*/', '', $url), '/');

		return explode('/', $url);
	}

	/*
	 * Get a URI segment
	 */
	private function segment($index_of_uri_segment)
	{
		if(isset($this->uri_segments[$index_of_uri_segment]))
			return $this->uri_segments[$index_of_uri_segment];
		//RESTRICTED SEGMENTS!!!
		/*
		if	(
				($this->uri_segments[0] == "user")
			||	($this->uri_segments[0] == "message")
			return false;
		*/

		return false;
	}
	/***************	-CRUD FUNCTIONS-	****************/

	/*
	 * Handles a POST and inserts into the database
	 */
	private function CREATE()
	{
		$table = $this->segment(0);

		if(!$table || !isset($this->db_structure[$table])){
			$this->response["error"] = array(
				'message' => 'Not Found',
				'code' => 404
			);
		}

		if($data = $this->_post())
		{

			$value_array  = array();
			foreach(array_values($data) as $value)
			{
				$value_array[] = "'{$value}'";
			}

			$sql =	"INSERT INTO " .$table. "(" .implode(", ", array_keys($data)). ")
					 VALUES (" .implode(", ", $value_array). ")"
					;

			$this->db_connection->query($sql);
			$this->response["'success"] = array(
				'message' => 'Success',
				'code' => 200
			);
		} 
		else 
		{
			$this->response["error"] = array(
				'message' => 'No Content',
				'code' => 204
			);
		}
	}

	/*
	 * Handles a GET and reads from the database
	 */
	private function READ()
	{
		$table = $this->segment(0);
		$id = intval($this->segment(1));

		if(!$table || !isset($this->db_structure[$table])){
			$this->response["error"] = array(
				'message' => 'Not Found',
				'code' => 404
			);
		}

		if($id && is_int($id)) {
			$index = 'id';
			if(isset($this->table_index[$table])) $index = $this->table_index[$table];

			$sql =	"SELECT * 
					 FROM " .$table. "
					 WHERE ".$index. "=" .$id. "
					 ORDER BY "._get('order_by'). " " .strtoupper($this->_get('order'))."
					 LIMIT".intval($this->_get('limit')). " , " .intval($this->_get('offset'))
					;
			if($response = $this->db_connection->query($sql)->fetch_array()){
			}
			else
			{
				$this->response["error"] = array(
					'message' => 'No Content',
					'code' => 204
				);
			}
		}
		else
		{
			$sql =	"SELECT *
					 FROM " .$table. "
					 ORDER BY"._get('order_by'). " , " .strtoupper($this->_get('order'))."
					 LIMIT".intval($this->_get('limit')). " , " .intval($this->_get('offset'))
					;
			if($response = $this->db_connection->query($sql)->fetch_all())
			{
				$response = $this->db_connection->query($sql)->fetch_all();
			} else {
				$this->response["error"] = array(
					'message' => 'No Content',
					'code' => 204
				);
			}
		}
	}

	/*
	 * Handles a PUT and updates the database
	 */
	private function UPDATE()
	{
		$table = $this->segment(0);
		$id = intval($this->segment(1));

		if(!$table || !isset($this->db_structure[$table]) || !$id){
			$this->response["error"] = array(
				'message' => 'Not Found',
				'code' => 404
			);
		}

		$index = 'id';
		if(isset($this->table_index[$table])) $index = $this->table_index[$table];

		$sql =	"SELECT *
				 FROM " .$table."
				 WHERE " .$index. "=" .$id
				;
		if($response = $this->db_connection->query($sql)->fetch_array())
		{
			$data_array = array();
			foreach($this->_put() as $key => $value)
			{
				$data_array[] = "`{$key}`='{$value}'";
			}

			$sql =	"UPDATE " .$table. "
					 SET " .implode(", ", $data_array)."
					 WHERE " .$index. "=" .$id
					;

			$this->db_connection->query($sql);
			$this->response["'success"] = array(
				'message' => 'Success',
				'code' => 200
			);
		} 
		else 
		{
			$this->response["error"] = array(
				'message' => 'No Content',
				'code' => 204
			);
		}
	}

	/*
	 * Handles a DELETE and deletes from the database
	 */
	private function DELETE()
	{
		$table = $this->segment(0);
		$id = intval($this->segment(1));

		if(!$table || !isset($this->db_structure[$table]) || !$id){
			$this->response["error"] = array(
				'message' => 'Not Found',
				'code' => 404
			);
		}

		$index = 'id';
		if(isset($this->table_index[$table])) $index = $this->table_index[$table];

		$sql =	"SELECT *
				 FROM " .$table."
				 WHERE " .$index. "=" .$id
				;
		if($response = $this->db_connection->query($sql)->fetch_array())
		{
			$sql =	"DELETE FROM " .$table.
					"WHERE " .$index. "=" .$id
					;
			$this->db_connection->query($sql);
			$this->response["'success"] = array(
				'message' => 'Success',
				'code' => 200
			);
		} 
		else 
		{
			$this->response["error"] = array(
				'message' => 'No Content',
				'code' => 204
			);
		}
	}
	/***************	-CRUD FUNCTIONS-	****************/

	/***************	-HELPER FUNCTIONS-	****************/

	/*
	 * Helper function to retrieve $_GET variables
	 */
	private function _get($index)
	{
		if (!isset($index))
		{
			$index = '';
		}
		if($index)
		{
			if(isset($_GET[$index]) && $_GET[$index]) return strip_tags($_GET[$index]);
		} 
		else 
		{
			if(isset($_GET) && !empty($_GET)) return $_GET;
		}
		return false;
	}

	/*
	 * Helper function to retrieve $_POST variables
	 */
	private function _post($index)
	{
		if (!isset($index))
		{
			$index = '';
		}
		if($index){
			if(isset($_POST[$index]) && $_POST[$index]) return $_POST[$index];
		} 
		else 
		{
			if(isset($_POST) && !empty($_POST)) return $_POST;
		}
		return false;
	}

	/*
	 * Helper function to retrieve PUT variables
	 * Mixed Returns the contents of PUT as an array
	 */
	private function _put()
	{
		$output = array();
		parse_str(file_get_contents('php://input'), $output);
		return $output;
	}

	/***************	-HELPER FUNCTIONS-	****************/

}

?>
