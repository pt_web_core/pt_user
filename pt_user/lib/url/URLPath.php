<?php

class URLPath{

	private $request;

	public function __construct()
	{
		$this->request = $_SERVER['REQUEST_URI'];
	}
	public function __destruct()
	{
		unset($this->request);
	}
	public function assign(string $route, string $file) : bool
	{

		if($_SERVER['REQUEST_URI'] == $route)
		{

			$uri = trim( $this->request, "/" );

			$uri = explode("/", $uri);

			if($uri[0] == trim($route, "/")){

				array_shift($uri);
				$args = $uri;

				include $file;

			}
			return true;
		}
		
		return false;
	}

	public function get(string $route) : bool
	{
		if($_SERVER['REQUEST_URI'] == $route)
		{
			return true;
		}
		
		return false;

	}

}