<?php

class RESTAPIDeviceAccessTokenSignup
{
	public $response = array
    (
        "error" => array(),
        "caution" => array(),
        "good" => array(),

        "device" => array()
    );
	
    public function __construct($json_decoded_array)
    {
        //JSON GET NEW ACCESS TOKEN FROM SIGNUP

        if($this->isSetInJSONGetAccessTokenSignup($json_decoded_array))
        {
            //WORK ON ME!
            $this->createRESTAPIDeviceAccessTokenFromSignup
            (
                $json_decoded_array["user"]["username"],
                $json_decoded_array["user"]["email"],
                $json_decoded_array["user"]["password"],
                $json_decoded_array["user"]["firstname"],
                $json_decoded_array["user"]["lastname"],
                $json_decoded_array["user"]["date_of_birth"],
                $json_decoded_array["user"]["sex"],

                $json_decoded_array["device"]["device_id"],
                $json_decoded_array["device"]["device_name"]
            );
        }
		
        if( $this->isSetInREQUESTGetAccessTokenSignup())
        {
            $this->createRESTAPIDeviceAccessTokenFromSignup
            (
                $_REQUEST["username"],
                $_REQUEST["email"],
                $_REQUEST["password"],
                $_REQUEST["firstname"],
                $_REQUEST["lastname"],
                $_REQUEST["date_of_birth"],
                $_REQUEST["sex"],

                $_REQUEST["device_id"],
                $_REQUEST["device_name"]
            );
        }

    }

    private function createRESTAPIDeviceAccessTokenFromSignup(string $username, string $email, string $password, string $firstname, string $lastname, string $date_of_birth, string $sex, string $device_id, string $device_name) : void
    {
        $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if (!$this->db_connection->set_charset("utf8"))
        {
            $response['error'] = array(
                'message' => " I'm a teapot (RFC 2324), ".$this->db_connection->error." ",
                'code' => 418
            );
        }

        if (!$this->db_connection->connect_errno)
        {
            $date_of_birth = date('Y-m-d', strtotime($date_of_birth)); //MySQL_DATETIME 'Y-m-d H:i:s'


            $username = $this->db_connection->real_escape_string($username);
            $email = $this->db_connection->real_escape_string($email);

            $sql = "SELECT id, password_hash
					FROM user
					WHERE username = '" .$username. "'
					AND email = '" .$email. "'
					AND firstname = '" .$firstname. "'
					AND lastname = '" .$lastname. "'
					AND date_of_birth = '" .$date_of_birth. "'
					AND sex = '" .$sex. "';" ;

            $sighnup_check = $this->db_connection->query($sql);
            if ($sighnup_check->num_rows == 1)
            {
                $response_row = $sighnup_check->fetch_object();
                if (password_verify($password, $response_row->password_hash))
                {

                    $id = $response_row->id;

                    //ADD NEW DEVICE TO DATABASE

                    $unique = SITE_UNIQUE_KEY;
                    $first_login_date = date("Y-m-d H:i:s");
                    $new_access_token = sha1( ($unique * sha1($id)).'=='.$device_id );

                    $sql = "INSERT INTO device (id, access_token, first_login_date, device_id, device_name)
							VALUES('" . $id . "', '" . $new_access_token . "', '" . $first_login_date . "', '" . $device_id . "', '" . $device_name . "');";

                    $this->db_connection->query($sql);
					
					$this->response["device"]["authorized"] = true;
                    $this->response["device"]["device_id"] = $device_id;
                    $this->response["device"]["device_name"] = $device_name;
                    $this->response["device"]["access_token"] = $new_access_token;

					$this->getSession($new_access_token);// set the session ID
					
					$_SESSION['id'] =  $id;
                    $_SESSION['username'] = $username;
                    $_SESSION['email'] = $email;
                    $_SESSION['login_status'] = 1;
                }					
				else
				{
					$this->response["error"] = array(
						'message' => 'Unauthorized, Wrong password. Try again.',
						'code' => 401
					);
				}
            }
        }
    }

    private function isSetInJSONGetAccessTokenSignup($json_decoded_array) : bool
    {
		if
        (
            isset($json_decoded_array["user"]["username"]) &&
            isset($json_decoded_array["user"]["email"]) &&
            isset($json_decoded_array["user"]["password"]) &&
            isset($json_decoded_array["user"]["firstname"]) &&
            isset($json_decoded_array["user"]["lastname"]) &&
            isset($json_decoded_array["user"]["date_of_birth"]) &&
            isset($json_decoded_array["user"]["sex"]) &&
            isset($json_decoded_array["device"]["device_id"]) &&
            isset($json_decoded_array["device"]["device_name"])
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function isSetInREQUESTGetAccessTokenSignup() : bool
    {
        if(
            isset($_REQUEST["username"]) &&
            isset($_REQUEST["email"]) &&
            isset($_REQUEST["password"]) &&
            isset($_REQUEST["firstname"]) &&
            isset($_REQUEST["lastname"]) &&
            isset($_REQUEST["date_of_birth"]) &&
            isset($_REQUEST["sex"]) &&

            isset($_REQUEST["device_id"]) &&
            isset($_REQUEST["device_name"])
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private function getSession(string $access_token) : void		//Session_ID should be ACCESS_TOKEN
    {
        if(session_id() != $access_token)
        {
            session_id($access_token);								// set the session ID
            session_name('persistence');
            session_start();
        }
		else
		{
			session_id($access_token);								// set the session ID
			session_start();
		}
    }
}