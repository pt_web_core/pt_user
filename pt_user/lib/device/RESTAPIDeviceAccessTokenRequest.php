<?php

class RESTAPIDeviceAccessTokenRequest
{
	private $db_connection;
	public $response = array
    (
        "error"  => array(),
        "caution"  => array(),
        "good"  => array(),

        "device"  => array()
    );
	
    function __construct($json_decoded_array)
    {
        // REQUEST GET NEW ACCESS TOKEN
        if ( $this->isSetInREQUESTGetAccessToken() )
        {
            $this->createRESTAPIDeviceAccessTokenRequest
			(
				$_REQUEST["id"] ,
				$_REQUEST["activation_key"],
				$_REQUEST["device_id"],
				$_REQUEST["device_name"],
				$_REQUEST["access_token"]
			);
        }

        //JSON GET NEW ACCESS TOKEN
        if ( $this->isSetInJSONGetAccessToken($json_decoded_array) )
        {
            $this->createRESTAPIDeviceAccessTokenRequest
            (
                $json_decoded_array["user"]["id"],
                $json_decoded_array["user"]["activation_key"],
                $json_decoded_array["device"]["device_id"],
                $json_decoded_array["device"]["device_name"],
                $json_decoded_array["device"]["access_token"]
            );
        }
    }



    private function createRESTAPIDeviceAccessTokenRequest( $id, $activation_key, $device_id, $device_name, $access_token )
    {

        $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if (!$this->db_connection->set_charset("utf8"))
        {
            $this->response["error"] = array(
                'message' => " I'm a teapot (RFC 2324), ".$this->db_connection->error." ",
                'code' => 418
            );
        }
        if (!$this->db_connection->connect_errno)
        {
			
			//CHECK USER ACTIVATION KEY
            $sql = "SELECT * FROM user 
					WHERE id = '" . $id . "' 
					AND activation_key = '" . $activation_key . "';";

            $query_check_user_activation_key = $this->db_connection->query($sql);
			
			if($access_token == "request")
			{
                if ($query_check_user_activation_key->num_rows == 1)
                {
                    //ADD NEW DEVICE TO DATABASE

                    $unique = SITE_UNIQUE_KEY;
                    $first_login_date = date("Y-m-d H:i:s");
                    $new_access_token = sha1( ($unique * sha1($id)).'=='.$device_id );

                    $sql = "INSERT INTO device (id, access_token, first_login_date, device_id, device_name)
							VALUES('" . $id . "', '" . $new_access_token . "', '" . $first_login_date . "', '" . $device_id . "', '" . $device_name . "');";

                    $this->db_connection->query($sql);
					
                    $this->response["device"]["device_id"] = $device_id;
                    $this->response["device"]["device_name"] = $device_name;
                    $this->response["device"]["access_token"] = $new_access_token;

                }
				else
				{
					$this->response["error"] = array(
					'message' => " User id or activation_key is invalid, access_token request will not be granted.",
					'code' => 400
					);
				}
			}
			else // IF $access_token mismatched
			{
				if ($query_check_user_activation_key->num_rows == 1)
				{
					//CHECK IF DEVICE EXISTS
					$sql = "SELECT * FROM device 
									WHERE id = '" . $id . "' 
									AND device_id = '" . $device_id . "'
									AND device_name = '" . $device_name . "';";
					$query_check_if_device_exists = $this->db_connection->query($sql);
					if ($query_check_if_device_exists->num_rows == 1)
					{
						$response_row = $query_check_if_device_exists->fetch_object();

						$this->response["device"]["device_id"]	=	$response_row->device_id;;
						$this->response["device"]["device_name"]	=	$response_row->device_name;
						$this->response["device"]["access_token"]	=	$response_row->access_token;
							
						$this->response["success"] = array(
								'message' => 'Success',
								'code' => 200
						);
					}
				}
				else
				{
					$this->response["error"] = array(
					'message' => " User id or activation_key is invalid, access_token can not be updated.",
					'code' => 400
					);
				}
				
            }
        }
        else
        {
            $this->response["error"] = array(
                'message' => " I'm a teapot (RFC 2324), Looks like a Database connection problem.",
                'code' => 418
            );
        }
    }

    private function isSetInREQUESTGetAccessToken()
    {
        if(
            isset($_REQUEST["id"]) &&							// for device table id column for sync with user table
            isset($_REQUEST["activation_key"]) &&
            !isset($_REQUEST["authorized"]) &&
            isset($_REQUEST["device_id"]) &&
            isset($_REQUEST["device_name"]) &&
            isset($_REQUEST["access_token"])
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function isSetInJSONGetAccessToken($json_decoded_array)
    {
        if(
            isset($json_decoded_array["user"]["id"]) &&				// for device table id column for sync with user table
            isset($json_decoded_array["user"]["activation_key"]) &&
			!isset($json_decoded_array["device"]["authorized"]) &&
            isset($json_decoded_array["device"]["device_id"]) &&
            isset($json_decoded_array["device"]["device_name"]) &&
            isset($json_decoded_array["device"]["access_token"])
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}