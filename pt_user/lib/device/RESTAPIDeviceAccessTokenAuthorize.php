<?php

class RESTAPIDeviceAccessTokenAuthorize
{
	private $db_connection;
	public $response = array
    (
        "error"  => array(),
        "caution"  => array(),
        "good"  => array(),

        "device"  => array()
    );
	
	private $is_authorized;							//bool true/false
	
	public function __construct($json_decoded_array) 
	{

		//JSON CHECK ACCESS TOKEN
		if ( $this->isSetInJSONSetAccessToken($json_decoded_array) )
		{
			if ( ($this->checkRESTAPIDeviceAccessTokenAuthorize( 
														$json_decoded_array["user"]["id"],
														$json_decoded_array["device"]["access_token"],
														$json_decoded_array["device"]["authorized"],
														$json_decoded_array["device"]["device_id"], 
														$json_decoded_array["device"]["device_name"] 
														) == true 
														
														&& null !== session_id($json_decoded_array["device"]["access_token"]))  
														|| $this->isAuthorized() == true
			)
			{
				$this->is_authorized = true;
				
				$this->response["device"]["authorized"] = true;
				
				$this->response["device"]["access_token"] =		$json_decoded_array["device"]["access_token"];
				$this->response["device"]["device_id"] =			$json_decoded_array["device"]["device_id"];
				$this->response["device"]["device_name"] =		$json_decoded_array["device"]["device_name"];

                $this->getSession($json_decoded_array["device"]["access_token"]);	// set the session ID
			}
		}
		// URL POST OR COOKIE
		else if ( $this->isSetInREQUESTSetAccessToken() )
		{
			if ( $this->checkRESTAPIDeviceAccessTokenAuthorize( 
														$_REQUEST["id"], 
														$_REQUEST["access_token"], 
														$_REQUEST["authorized"], 
														$_REQUEST["device_id"], 
														$_REQUEST["device_name"] 
														) == true 
														&& null !== session_id($_REQUEST["access_token"])
														|| $this->isAuthorized() == true
				)
			{
				$this->is_authorized = true;
				
				$this->response["device"]["authorized"] = true;
								
				$this->response["device"]["access_token"] =		$_REQUEST["access_token"];
				$this->response["device"]["device_id"] =			$_REQUEST["device_id"];
				$this->response["device"]["device_name"] =		$_REQUEST["device_name"];
				
				$this->getSession($_REQUEST["access_token"]);					// set the session ID
			}
		}
		else
		{
			
			$this->is_authorized = false;
			if($_REQUEST["authorized"] == "check" || isset($json_decoded_array["device"]["authorized"]) == "check")
			{
				$this->response["device"]["authorized"] = false;				
			}

		}
		
	}

	private function checkRESTAPIDeviceAccessTokenAuthorize($id , $access_token, $authorized, $device_id, $device_name)
	{
		if($authorized == "check")
		{
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if (!$this->db_connection->set_charset("utf8")) 
			{
				$this->response["error"] = array(
								'message' => " I'm a teapot (RFC 2324), ".$this->db_connection->error." ",
								'code' => 418
								);
			}
			if (!$this->db_connection->connect_errno) 
			{
				
				$sql = "SELECT * FROM device 
						WHERE id = '".$id."' 
						AND access_token = '".$access_token."'  
						AND device_id = '".$device_id."'  
						AND device_name = '".$device_name."';";

				$query_check_access_token = $this->db_connection->query($sql);
				if ($query_check_access_token->num_rows == 1) 
				{
					$sql = "SELECT id, username, password_hash, email, firstname, lastname, date_of_birth, gender, activated, activation_key
							FROM user
							WHERE id = '" .$id. "';";
					$login_check = $this->db_connection->query($sql);
					
					if ($login_check->num_rows == 1) 
					{
						session_id($access_token);
						session_start();
						
						$response_row = $login_check->fetch_object();
						
						$_SESSION['id'] =  $response_row->id;
						$_SESSION['username'] = $response_row->username;
						$_SESSION['email'] = $response_row->email;
						$_SESSION['login_status'] = 2;
					}
					return true;
				}
				else 
				{
					return false; 
				}
				
			} 
			else 
			{
				$this->response["error"] = array(
								'message' => " I'm a teapot (RFC 2324), Looks like a Database connection problem.",
								'code' => 418
								); 
			}
		}
	}
	public function isAuthorized() : bool
	{
		if(isset($is_authorized))
		{
			return $is_authorized;
		}
		return false;
	}

	private function isSetInJSONSetAccessToken($json_decoded_array) : bool
	{
		if(
			isset($json_decoded_array["user"]["id"]) &&				// for device table id column for sync with user table
			isset($json_decoded_array["user"]["activation_key"]) &&
			isset($json_decoded_array["device"]["authorized"]) &&
			isset($json_decoded_array["device"]["device_id"]) &&
			isset($json_decoded_array["device"]["device_name"]) &&
			isset($json_decoded_array["device"]["access_token"]) && $json_decoded_array["device"]["access_token"] != "request"
		)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	private function isSetInREQUESTSetAccessToken() : bool
	{
		if(
			isset($_REQUEST["id"]) &&								// for device table id column for sync with user table
			isset($_REQUEST["activation_key"]) &&
			isset($_REQUEST["authorized"]) &&
			isset($_REQUEST["device_id"]) &&
			isset($_REQUEST["device_name"]) &&
			isset($_REQUEST["access_token"]) && $_REQUEST["access_token"] != "request"
		)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

    private function getSession(string $access_token) : void				//Session_ID should be ACCESS_TOKEN
    {
        if(session_id() != $access_token)
        {
            session_id($access_token);										// set the session ID
            session_name('persistence');
            session_start();
        }
		else
		{
			session_id($access_token);										// set the session ID
			session_start();
		}
    }

	public static function getDeviceSession($json_decoded_array) : void		//Session_ID should be ACCESS_TOKEN
    {
		$access_token;
		
		if(isset($_REQUEST["access_token"]))
		{
			$access_token = $_REQUEST["access_token"];
		}
		
		if(isset($json_decoded_array["device"]["access_token"]))
		{
			$access_token = $json_decoded_array["device"]["access_token"];
		}
		
        if(session_id() != strval($access_token))
        {
            session_id(strval($access_token));									// set the session ID
            session_name('persistence');
            session_start();
        }
		else
		{
			session_id(strval($access_token));									// set the session ID
			session_start();
		}
	
	}	
	public static function removeDeviceSessionFile(string $access_token) : void
	{
		unlink( realpath(session_save_path().'/'."sess_".$access_token) );
	}
	
}

?>