<?php

class Device
{	
	private $db_connection;
	private $row_limit;
	private $number_of_pages;	
	
	private $has_devices;
	
	protected $page;
	protected $device_id;
	



	public $response = array
    (
        "error"  => array(),
        "caution"  => array(),
        "good"  => array(),

        "device"  => array()
    );
	
	public function __construct() 
	{
		$this->page = $_REQUEST['page'];
		
		$this->queryDevice();
		if (isset($_REQUEST['remove_device']))
		{
			$this->device_id = $_REQUEST['remove_device'];
			$this->deactivateDevice();			
		}

	}
	public function __destruct()
	{
		unset($this->db_connection);
		unset($this->page);
		unset($this->row_limit);
		unset($this->number_of_pages);
		unset($this->has_devices);
		unset($this->device_id);
		unset($this->response);
	}
	
	protected function queryDevice(bool $is_rest = false) : void
	{
		$id = $_SESSION['id'];
		
		$this->row_limit = 50;
		
		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
		if (!$this->db_connection->connect_errno) 
		{

			$abs_page = abs($this->page);
			
			if($abs_page > 1)
			{
				$page_offset = $abs_page * $this->row_limit;	
			}
			else
			{
				$page_offset = 0;
			}

			/******************************************************************************/
			$sql = "SELECT id, access_token, first_login_date, device_id, device_name
					FROM device
					WHERE id = '" .$id. "'
					ORDER BY first_login_date DESC;

					;";
			//OR date_of_birth BETWEEN '".$age_to."' AND '"$age_from"'					
			$check_pages = $this->db_connection->query($sql);		
			/******************************************************************************/
			$sql = "SELECT id, access_token, first_login_date, device_id, device_name
					FROM device
					WHERE id = '" .$id. "'
					ORDER BY first_login_date DESC
					LIMIT ".$this->row_limit." OFFSET " .$page_offset. ";

					;";
			//OR date_of_birth BETWEEN '".$age_to."' AND '"$age_from"'
			$check_device = $this->db_connection->query($sql);
			if ($check_device->num_rows > 0)
			{
				$this->has_devices = true;
				$this->number_of_pages = (floor($check_pages->num_rows/$this->row_limit));
				if($this->number_of_pages < 0)
				{
					$this->number_of_pages = 0;
				}
				
			    $index = 0;
				
				$this->response['device']['pages'] = $this->number_of_pages;
				$this->response['device']['current_page'] = $abs_page;
				foreach ( $check_device as  $response_row)
				{
			
                    $this->response['device'][$index]['id'] = $response_row["id"];
                    $this->response['device'][$index]['access_token'] = $response_row["access_token"];
                    $this->response['device'][$index]['first_login_date'] = $response_row["first_login_date"];
                    $this->response['device'][$index]['device_id'] = $response_row["device_id"];
                    $this->response['device'][$index]['device_name'] = $response_row["device_name"];

                    $index++;
				}
			}
			else
			{
				$this->has_devices = false;
			}
		} 
	}
	
	protected function deactivateDevice(bool $is_rest = false) : void
	{
		$id = $_SESSION['id'];

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
		if (!$this->db_connection->connect_errno) 
		{
			$sql = "SELECT id, access_token, first_login_date, device_id, device_name
					FROM device
					WHERE id = '" .$id. "' 
					AND device_id = '" .$this->device_id. "';";
			$deactivate_check = $this->db_connection->query($sql);
			if ($deactivate_check->num_rows == 1) 
			{
			
				$sql = "DELETE FROM device
						WHERE id = '" .$id. "' 
						AND device_id = '" .$this->device_id. "';";
					
				$query_new_deactivation = $this->db_connection->query($sql);
				
				foreach ( $deactivate_check as  $response_row)
				{
					unlink( realpath(session_save_path().'/'."sess_".$response_row["access_token"]) );
				}
			}
			else { $this->error[] = "This device does not exist."; }
		} 
		else { $this->error[] = "Database connection problem."; }
	}
	
		
	public function getNumberOfPages() : int
	{
		if (isset($this->number_of_pages))
		{
			return $this->number_of_pages;			
		}
		else
		{
			return 0;
		}

	}	
	
	public function hasDevices() : bool
	{
		if ($this->has_devices)
		{
			return true;			
		}
		return false;

	}
}

?>