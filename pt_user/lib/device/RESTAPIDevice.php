<?php
	
include_once("Device.php");

class RESTAPIDevice extends Device
{

	public function __construct($json_decoded_array) 
	{
		/*******************************************************/
		if ($this->isDeactivateSetInJSON($json_decoded_array))
		{
			$this->deactivateDevice();
		}
		if ($this->isQuerySetInJSON($json_decoded_array))
		{
			$this->queryDevice();	
		}
		/*******************************************************/
		if ($this->isDeactivateSetInREQUEST())
		{
			$this->deactivateDevice();			
		}
		if ($this->isQuerySetInREQUEST())
		{
			$this->queryDevice();	
		}
		/*******************************************************/
	}
	public function __destruct()
	{
		parent::__destruct();
	}
	
	protected function queryRESTAPIDevice() : void
	{
		parent::queryDevice(true);
	}
	
	public function deactivateRESTAPIDevice() : void
	{
		parent::deactivateDevice(true);
	}
	
	private function isQuerySetInJSON($json_decoded_array) : bool
	{
		$is_true = false;
			
		if (isset($json_decoded_array['device']['query']) && $json_decoded_array['device']['query'] == "true") {
			$is_true = true;
		}
		if (isset($json_decoded_array['device']['page'])) {
			$this->page = intval($json_decoded_array['device']['page']);
		} else {
			$this->page = 1;
		}
		
		return $is_true;
	}
	private function isQuerySetInREQUEST() : bool
	{
		$is_true = false;
			
		if (isset($_REQUEST['query']) && $_REQUEST['query'] == "true") {
			$is_true = true;
		}
		if (isset($_REQUEST['page'])) {
			$this->page = intval($_REQUEST['page']);
		} else {
			$this->page = 1;
		}
			
		return $is_true;
	}
	
	private function isDeactivateSetInJSON($json_decoded_array) : bool
	{
		$is_true = false;
			
		if (isset($json_decoded_array['device']['remove_device'])) {
			$this->device_id = $json_decoded_array['device']['remove_device'];
			$is_true = true;
		}
		
		return $is_true;
	}
	private function isDeactivateSetInREQUEST() : bool
	{
		$is_true = false;
			
		if (isset($_REQUEST['remove_device'])) {
			$this->device_id = $_REQUEST['remove_device'];
			$is_true = true;
		}
			
		return $is_true;
	}
}

?>