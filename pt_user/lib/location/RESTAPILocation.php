<?php
	
include_once("Location.php");

class RESTAPILocation extends Location
{
	private $latitude;
	private $longitude;

	public function __construct($json_decoded_array) 
	{
		/*******************************************************/
		if ($this->isLocationSetInJSON($json_decoded_array))
		{
			$this->getRESTAPILocationFromCoordinates($this->latitude, $this->longitude);
		}
		/*******************************************************/
		else if ($this->isLocationSetInREQUEST())
		{
			$this->getRESTAPILocationFromCoordinates($this->latitude, $this->longitude);
		}
		/*******************************************************/
		else
		{
			$this->getRESTAPILocationFromIP();	
		}
		/*******************************************************/
	}
	public function __destruct()
	{
		unset($this->latitude);
		unset($this->longitude);
		parent::__destruct();
	}
	
	protected function getRESTAPILocationFromIP() : void
	{
		parent::getLocationFromIP(true);
	}
	
	public function getRESTAPILocationFromCoordinates(string $latitude, string $longitude) : void
	{
		parent::getLocationFromCoordinates($latitude, $longitude, true);
	}

	private function isLocationSetInJSON($json_decoded_array) : bool
	{
		if 
		(
			isset($json_decoded_array['location']['latitude']) &&
			isset($json_decoded_array['location']['longitude'])
		) 
		{
			$this->latitude = strval($json_decoded_array['location']['latitude']);
			$this->longitude = strval($json_decoded_array['location']['longitude']);
			
			return true;
		}
		else
		{
			return false;
		}
	}
	private function isLocationSetInREQUEST() : bool
	{	
		if (
			isset($_REQUEST['latitude']) &&
			isset($_REQUEST['longitude'])
		) 
		{
			$this->latitude = strval($_REQUEST['latitude']);
			$this->longitude = strval($_REQUEST['longitude']);
			
			return true;
		}
		else
		{
			return false;
		}
	}
}

?>