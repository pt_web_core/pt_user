<?php

function ip2long_v6(string $ip) : string
{
	return strval( gmp_import( inet_pton($ip) ) );
}
	
function long2ip_v6(string $int) : string
{
	return strval(inet_ntop(str_pad(gmp_export($int), 16, "\0", STR_PAD_LEFT)));
}

class Location
{	
	private $db_connection;


	public $response = array
    (
        "error"  => array(),
        "caution"  => array(),
        "good"  => array(),
		
		"location"  => array()
    );
	
	public function __construct() 
	{
		if($_REQUEST['latitude'] && $_REQUEST['longitude'])
		{
			$this->getLocationFromCoordinates
			(
				$_REQUEST['latitude'] ,
				$_REQUEST['longitude']
			);
		}
		else if($_REQUEST['ip'])
		{
			$this->getIPInfo
			(
				$_REQUEST['ip'] 
			);
		}
		else
		{
			$this->getLocationFromIP();	
		}
		
	}
	public function __destruct()
	{
		unset($this->db_connection);
		unset($this->response);
	}
	
	protected function getLocationFromIP(bool $is_rest = false) : void
	{
		$ip = Location::getIPAddress();
		$is_ipv6 = Location::isIPV6($ip);
		
		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
		if (!$this->db_connection->connect_errno) 
		{
			if($is_ipv6 == false)
			{
				$iplong = ip2long($ip);
				
				$sql = "SELECT ip, country_code, country_name, region_name, city_name, latitude, longitude
						FROM ipv4_location_user_correction
						WHERE ip='" .$iplong. "';";
				
				$ipv4_check = $this->db_connection->query($sql);
				if ($ipv4_check->num_rows == 1)
				{
					$response_row = $ipv4_check->fetch_object();

					/*****************************************/
					$this->response["location"]["country_code"] = $response_row->country_code;
					$this->response["location"]["country_name"] = $response_row->country_name;
					$this->response["location"]["region_name"] = $response_row->region_name;
					$this->response["location"]["city_name"] = $response_row->city_name;
					$this->response["location"]["latitude"] = $response_row->latitude;
					$this->response["location"]["longitude"] = $response_row->longitude;
					/*****************************************/
				}
				else
				{
					
					$sql = "SELECT ip_from, ip_to
							FROM ipv4_proxy_lookup
							WHERE '" .$iplong. "' BETWEEN  ipv4_proxy_lookup.ip_from AND ipv4_proxy_lookup.ip_to;";
					$ipv4_check = $this->db_connection->query($sql);
					if ($ipv4_check->num_rows == 1)
					{
						$country_code = "XX";
						$country_name = "Limbo";
						$region_name = "Bermuda Triangle";
						$city_name = "Digital Purgatory";
						$latitude = "25.000000";
						$longitude = "-71.000000";	

						$this->response['error'][] = "Proxy Servers are blocked by this service.";						
					}
					else
					{
						$sql = "SELECT ip_from, ip_to, country_code, country_name, region_name, city_name, latitude, longitude
								FROM ipv4_location_lookup
								WHERE '" .$iplong. "' BETWEEN  ipv4_location_lookup.ip_from AND ipv4_location_lookup.ip_to;";
						
						$ipv4_check = $this->db_connection->query($sql);
						if ($ipv4_check->num_rows == 1)
						{
							$response_row = $ipv4_check->fetch_object();
							
							$country_code = $response_row->country_code;
							$country_name = $response_row->country_name;
							$region_name = $response_row->region_name;
							$city_name = $response_row->city_name;
							$latitude = $response_row->latitude;
							$longitude = $response_row->longitude;
							
							/*****************************************/
							$this->response["location"]["country_code"] = $country_code;
							$this->response["location"]["country_name"] = $country_name;
							$this->response["location"]["region_name"] = $region_name;
							$this->response["location"]["city_name"] = $city_name;
							$this->response["location"]["latitude"] = $latitude;
							$this->response["location"]["longitude"] = $longitude;
							/*****************************************/
						}	
					}						
					
					if
					( 
						isset($_SESSION['id']) && 
						isset($country_code) && isset($country_name) &&
						isset($region_name) && isset($city_name) &&
						isset($city_name) && 
						isset($latitude) && isset($longitude)
					)
					{
						$id = $_SESSION['id'];
								
						$sql = "INSERT INTO user_last_location (id, country_code, country_name, region_name, city_name, latitude, longitude)
								VALUES('" . $id . "', '" . $country_code . "', '" . $country_name . "', '" . $region_name . "', '" . $city_name . "', '" . $latitude . "', '" . $longitude . "')
								ON DUPLICATE KEY 
								UPDATE 
								country_code='". $country_code . "', country_name='". $country_name . "',  
								region_name='". $region_name . "', city_name='". $city_name . "',  
								latitude='". $latitude . "', longitude='". $longitude . "'
								;";
						$this->db_connection->query($sql);
					}
				}
			}
			else
			{
				$iplong = ip2long_v6($ip);		
				
				$sql = "SELECT ip, country_code, country_name, region_name, city_name, latitude, longitude
						FROM ipv6_location_user_correction
						WHERE ip='" .$iplong. "';";
				
				$ipv6_check = $this->db_connection->query($sql);
				if ($ipv6_check->num_rows == 1)
				{
					$response_row = $ipv6_check->fetch_object();

					/*****************************************/
					$this->response["location"]["country_code"] = $response_row->country_code;
					$this->response["location"]["country_name"] = $response_row->country_name;
					$this->response["location"]["region_name"] = $response_row->region_name;
					$this->response["location"]["city_name"] = $response_row->city_name;
					$this->response["location"]["latitude"] = $response_row->latitude;
					$this->response["location"]["longitude"] = $response_row->longitude;
					/*****************************************/
				}
				else
				{	
					$sql = "SELECT ip_from, ip_to
							FROM ipv6_proxy_lookup
							WHERE '" .$iplong. "' BETWEEN  ipv6_proxy_lookup.ip_from AND ipv6_proxy_lookup.ip_to;";
							$ipv6_check = $this->db_connection->query($sql);
					if ($ipv6_check->num_rows == 1)
					{
						$country_code = "XX";
						$country_name = "Limbo";
						$region_name = "Bermuda Triangle";
						$city_name = "Digital Purgatory";
						$latitude = "25.000000";
						$longitude = "-71.000000";	

						$this->response['error'][] = "Proxy Servers are blocked by this service.";							
					}
					else
					{					
						$sql = "SELECT ip_from, ip_to, country_code, country_name, region_name, city_name, latitude, longitude
								FROM ipv6_location_lookup
								WHERE '" .$iplong. "' BETWEEN  ipv6_location_lookup.ip_from AND ipv6_location_lookup.ip_to;";
						
						$ipv6_check = $this->db_connection->query($sql);
						if ($ipv6_check->num_rows == 1)
						{
							$response_row = $ipv6_check->fetch_object();
							
							$country_code = $response_row->country_code;
							$country_name = $response_row->country_name;
							$region_name = $response_row->region_name;
							$city_name = $response_row->city_name;
							$latitude = $response_row->latitude;
							$longitude = $response_row->longitude;
							$zip_code = $response_row->zip_code;
							$time_zone = $response_row->time_zone;
							
							/*****************************************/
							$this->response["location"]["country_code"] = $country_code;
							$this->response["location"]["country_name"] = $country_name;
							$this->response["location"]["region_name"] = $region_name;
							$this->response["location"]["city_name"] = $city_name;
							$this->response["location"]["latitude"] = $latitude;
							$this->response["location"]["longitude"] = $longitude;
							/*****************************************/
						}
					}							
					if
					( 
						isset($_SESSION['id']) && 
						isset($country_code) && isset($country_name) &&
						isset($region_name) && isset($city_name) &&
						isset($city_name) && 
						isset($latitude) && isset($longitude)
					)
					{
						$id = $_SESSION['id'];
								
						$sql = "INSERT INTO user_last_location (id, country_code, country_name, region_name, city_name, latitude, longitude)
								VALUES('" . $id . "', '" . $country_code . "', '" . $country_name . "', '" . $region_name . "', '" . $city_name . "', '" . $latitude . "', '" . $longitude . "')
								ON DUPLICATE KEY 
								UPDATE 
								country_code='". $country_code . "', country_name='". $country_name . "',  
								region_name='". $region_name . "', city_name='". $city_name . "',  
								latitude='". $latitude . "', longitude='". $longitude . "'
								;";
						$this->db_connection->query($sql);					
					}
				}
			}
		} 
	}	
	protected function getLocationFromCoordinates(string $latitude, string $longitude, bool $is_rest = false) : void
	{

		$ip = Location::getIPAddress();
		$is_ipv6 = Location::isIPV6($ip);

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
		if (!$this->db_connection->connect_errno) 
		{
			if($is_ipv6 == false)
			{
				$iplong = ip2long($ip);
				
								
				$sql = "SELECT ip, country_code, country_name, region_name, city_name, latitude, longitude
						FROM ipv4_location_user_correction
						WHERE latitude = '" .$latitude. "' 
						AND longitude = '" .$longitude. "' 
						;";
				
				$ipv4_check = $this->db_connection->query($sql);
				if ($ipv4_check->num_rows == 1)
				{
					$response_row = $ipv4_check->fetch_object();
					/*****************************************/
					$this->response["location"]["country_code"] = $response_row->country_code;
					$this->response["location"]["country_name"] = $response_row->country_name;
					$this->response["location"]["region_name"] = $response_row->region_name;
					$this->response["location"]["city_name"] = $response_row->city_name;
					$this->response["location"]["latitude"] = $latitude;
					$this->response["location"]["longitude"] = $longitude;
					/*****************************************/
				}
				else
				{
					$sql = "SELECT
							*, (
							  6371 * acos (
							  cos ( radians(".$latitude.") )
							  * cos( radians( world_city_location_lookup.latitude ) )
							  * cos( radians( world_city_location_lookup.longitude ) - radians(".$longitude.") )
							  + sin ( radians(".$latitude.") )
							  * sin( radians( world_city_location_lookup.latitude ) )
							)
						) 
						AS distance
						FROM world_city_location_lookup
						HAVING distance < 30
						ORDER BY distance
						LIMIT 0 , 1;";

					$ipv4_check = $this->db_connection->query($sql);
					if ($ipv4_check->num_rows == 1)
					{
						$response_row = $ipv4_check->fetch_object();
						
						$country_code = $response_row->country_code;
						$country_name = $response_row->country_name;
						$region_name = $response_row->region_name;
						$city_name = $response_row->city_name;

						/*****************************************/
						$this->response["location"]["country_code"] = $country_code;
						$this->response["location"]["country_name"] = $country_name;
						$this->response["location"]["region_name"] = $region_name;
						$this->response["location"]["city_name"] = $city_name;
						$this->response["location"]["latitude"] = $latitude;
						$this->response["location"]["longitude"] = $longitude;
						/*****************************************/						
					if
					( 
						isset($_SESSION['id']) && 
						isset($country_code) && isset($country_name) &&
						isset($region_name) && isset($city_name) &&
						isset($city_name) && 
						isset($latitude) && isset($longitude)
					)
						{
							$id = $_SESSION['id'];

							$sql = "INSERT INTO user_last_location (id, country_code, country_name, region_name, city_name, latitude, longitude)
									VALUES('" . $id . "', '" . $country_code . "', '" . $country_name . "', '" . $region_name . "', '" . $city_name . "', '" . $latitude . "', '" . $longitude . "')
									ON DUPLICATE KEY 
									UPDATE 
									country_code='". $country_code . "', country_name='". $country_name . "',  
									region_name='". $region_name . "', city_name='". $city_name . "',  
									latitude='". $latitude . "', longitude='". $longitude . "'
									;";
							$this->db_connection->query($sql);
						
						}
						/****************************************************************************/
						if($is_rest == false)
						{
							$sql = "INSERT INTO ipv4_location_user_correction (ip, country_code, country_name, region_name, city_name, latitude, longitude)
									VALUES('" . $iplong . "', '" . $country_code . "', '" . $country_name . "', '" . $region_name . "', '" . $city_name . "', '" . $latitude . "', '" . $longitude . "')
									ON DUPLICATE KEY 
									UPDATE 
											country_code='". $country_code . "', country_name='". $country_name . "',  
											region_name='". $region_name . "', city_name='". $city_name . "',  
											latitude='". $latitude . "', longitude='". $longitude . "'
									;";
							$this->db_connection->query($sql);
						}
						/****************************************************************************/
					}					
				}
			}
			else
			{
				$iplong = ip2long_v6($ip);
				
				$sql = "SELECT
						*, (
						  6371 * acos (
						  cos ( radians(".$latitude.") )
						  * cos( radians( world_city_location_lookup.latitude ) )
						  * cos( radians( world_city_location_lookup.longitude ) - radians(".$longitude.") )
						  + sin ( radians(".$latitude.") )
						  * sin( radians( world_city_location_lookup.latitude ) )
						)
					) 
					AS distance
					FROM world_city_location_lookup
					HAVING distance < 30
					ORDER BY distance
					LIMIT 0 , 1;";

				$ipv6_check = $this->db_connection->query($sql);
				if ($ipv6_check->num_rows == 1)
				{
					$response_row = $ipv6_check->fetch_object();
					
					$country_code = $response_row->country_code;
					$country_name = $response_row->country_name;
					$region_name = $response_row->region_name;
					$city_name = $response_row->city_name;

					/*****************************************/
					$this->response["location"]["country_code"] = $country_code;
					$this->response["location"]["country_name"] = $country_name;
					$this->response["location"]["region_name"] = $region_name;
					$this->response["location"]["city_name"] = $city_name;
					$this->response["location"]["latitude"] = $latitude;
					$this->response["location"]["longitude"] = $longitude;
					/*****************************************/						
					if
					( 
						isset($_SESSION['id']) && 
						isset($country_code) && isset($country_name) &&
						isset($region_name) && isset($city_name) &&
						isset($city_name) && 
						isset($latitude) && isset($longitude)
					)
					{
						$id = $_SESSION['id'];

						$sql = "INSERT INTO user_last_location (id, country_code, country_name, region_name, city_name, latitude, longitude)
								VALUES('" . $id . "', '" . $country_code . "', '" . $country_name . "', '" . $region_name . "', '" . $city_name . "', '" . $latitude . "', '" . $longitude . "')
								ON DUPLICATE KEY 
								UPDATE 
								country_code='". $country_code . "', country_name='". $country_name . "',  
								region_name='". $region_name . "', city_name='". $city_name . "',  
								latitude='". $latitude . "', longitude='". $longitude . "'
								;";
						$this->db_connection->query($sql);
						
					}
					/****************************************************************************/
					$sql = "INSERT INTO ipv6_location_user_correction (ip, country_code, country_name, region_name, city_name, latitude, longitude)
							VALUES('" . $iplong . "', '" . $country_code . "', '" . $country_name . "', '" . $region_name . "', '" . $city_name . "', '" . $latitude . "', '" . $longitude . "')
							ON DUPLICATE KEY 
							UPDATE 
									country_code='". $country_code . "', country_name='". $country_name . "',  
									region_name='". $region_name . "', city_name='". $city_name . "',  
									latitude='". $latitude . "', longitude='". $longitude . "'
							;";
					$this->db_connection->query($sql);
					/****************************************************************************/
				}					
			}
		} 
	}

	protected function getIPInfo(string $ip, bool $is_rest = false) : void
	{
		$is_ipv6 = Location::isIPV6($ip);

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
		if (!$this->db_connection->connect_errno) 
		{
			if($is_ipv6 == false)
			{
				$iplong = ip2long($ip);
				
				$sql = "SELECT ip, country_code, country_name, region_name, city_name, latitude, longitude
						FROM ipv4_location_user_correction
						WHERE ip='" .$iplong. "';";
				
				$ipv4_check = $this->db_connection->query($sql);
				if ($ipv4_check->num_rows == 1)
				{
					$response_row = $ipv4_check->fetch_object();

					/*****************************************/
					$this->response["location"]["country_code"] = $response_row->country_code;
					$this->response["location"]["country_name"] = $response_row->country_name;
					$this->response["location"]["region_name"] = $response_row->region_name;
					$this->response["location"]["city_name"] = $response_row->city_name;
					$this->response["location"]["latitude"] = $response_row->latitude;
					$this->response["location"]["longitude"] = $response_row->longitude;
					/*****************************************/
				}
				else
				{
					$sql = "SELECT ip_from, ip_to
							FROM ipv4_proxy_lookup
							WHERE '" .$iplong. "' BETWEEN  ipv4_proxy_lookup.ip_from AND ipv4_proxy_lookup.ip_to;";
					$ipv4_check = $this->db_connection->query($sql);
					if ($ipv4_check->num_rows == 1)
					{
						$this->response["location"]["country_code"] = $response_row->country_code;
						$this->response["location"]["country_name"] = $response_row->country_name;
						$this->response["location"]["region_name"] = $response_row->region_name;
						$this->response["location"]["city_name"] = $response_row->city_name;
						$this->response["location"]["latitude"] = $response_row->latitude;
						$this->response["location"]["longitude"] = $response_row->longitude;						
					}
					else
					{
						$sql = "SELECT ip_from, ip_to, country_code, country_name, region_name, city_name, latitude, longitude
								FROM ipv4_location_lookup
								WHERE '" .$iplong. "' BETWEEN  ipv4_location_lookup.ip_from AND ipv4_location_lookup.ip_to;";
						
						$ipv4_check = $this->db_connection->query($sql);
						if ($ipv4_check->num_rows == 1)
						{
							$response_row = $ipv4_check->fetch_object();
							
							$this->response["location"]["country_code"] = $response_row->country_code;
							$this->response["location"]["country_name"] = $response_row->country_name;
							$this->response["location"]["region_name"] = $response_row->region_name;
							$this->response["location"]["city_name"] = $response_row->city_name;
							$this->response["location"]["latitude"] = $response_row->latitude;
							$this->response["location"]["longitude"] = $response_row->longitude;	
						}	
					}						
				}
			}
			else
			{
				$iplong = ip2long_v6($ip);		
				
				$sql = "SELECT ip, country_code, country_name, region_name, city_name, latitude, longitude
						FROM ipv6_location_user_correction
						WHERE ip='" .$iplong. "';";
				
				$ipv6_check = $this->db_connection->query($sql);
				if ($ipv6_check->num_rows == 1)
				{
					$response_row = $ipv6_check->fetch_object();
					$this->response["location"]["country_code"] = $response_row->country_code;
					$this->response["location"]["country_name"] = $response_row->country_name;
					$this->response["location"]["region_name"] = $response_row->region_name;
					$this->response["location"]["city_name"] = $response_row->city_name;
					$this->response["location"]["latitude"] = $response_row->latitude;
					$this->response["location"]["longitude"] = $response_row->longitude;

				}
				else
				{
					$sql = "SELECT ip_from, ip_to
							FROM ipv6_proxy_lookup
							WHERE '" .$iplong. "' BETWEEN  ipv6_proxy_lookup.ip_from AND ipv6_proxy_lookup.ip_to;";
							$ipv6_check = $this->db_connection->query($sql);
					if ($ipv6_check->num_rows == 1)
					{
						$response_row = $ipv6_check->fetch_object();
						$this->response["location"]["country_code"] = $response_row->country_code;
						$this->response["location"]["country_name"] = $response_row->country_name;
						$this->response["location"]["region_name"] = $response_row->region_name;
						$this->response["location"]["city_name"] = $response_row->city_name;
						$this->response["location"]["latitude"] = $response_row->latitude;
						$this->response["location"]["longitude"] = $response_row->longitude;						
					}
					else
					{
						$sql = "SELECT ip_from, ip_to, country_code, country_name, region_name, city_name, latitude, longitude
								FROM ipv6_location_lookup
								WHERE '" .$iplong. "' BETWEEN  ipv6_location_lookup.ip_from AND ipv6_location_lookup.ip_to;";
						
						$ipv6_check = $this->db_connection->query($sql);
						if ($ipv6_check->num_rows == 1)
						{
							$response_row = $ipv6_check->fetch_object();
							$this->response["location"]["country_code"] = $response_row->country_code;
							$this->response["location"]["country_name"] = $response_row->country_name;
							$this->response["location"]["region_name"] = $response_row->region_name;
							$this->response["location"]["city_name"] = $response_row->city_name;
							$this->response["location"]["latitude"] = $response_row->latitude;
							$this->response["location"]["longitude"] = $response_row->longitude;
						}
					}							
				}
			}
		} 
	}	
/*************************************************************************/
	public static function getIPAddress() : string
	{
		//whether ip is from share internet
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   
		  {
			return $_SERVER['HTTP_CLIENT_IP'];
		  }
		//whether ip is from proxy
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
		  {
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		  }
		//whether ip is from remote address
		else
		  {
			return $_SERVER['REMOTE_ADDR'];
		  }	
	}
	
	public static function isIPV6(string $ip) : bool
	{
		if (strpos($ip, ':') !== false) {
			return true;
		}
		else
		{
			return false;
		}
	}
}

?>