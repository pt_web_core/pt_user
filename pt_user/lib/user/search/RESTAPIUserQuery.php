<?php
include_once("UserQuery.php");

class RESTAPIUserQuery extends UserQuery
{
	private $username;
	private $email;
	private $firstname;
	private $lastname;
	private $sex;
	private $age_from;
	private $age_to;
	private $search_strict;
	private $page;
		
	function __construct($json_decoded_array)
	{
		if($this->isSetInJSON($json_decoded_array))
		{
			$this->queryRESTAPIUser
			(
				$this->username,
				$this->email,
				$this->firstname,
				$this->lastname,
				$this->sex,
				intval($this->age_from),
				intval($this->age_to),
				$this->search_strict,
				intval($this->page)
			);
		}

		if($this->isSetInREQUEST())
		{
			$this->queryRESTAPIUser
			(
				$this->username,
				$this->email,
				$this->firstname,
				$this->lastname,
				$this->sex,
				intval($this->age_from),
				intval($this->age_to),
				$this->search_strict,
				intval($this->page)
			);
		}

	}
		
	public function __destruct()
	{
		parent::__destruct();
		unset($this->username);
		unset($this->email);
		unset($this->firstname);
		unset($this->sex);
		unset($this->age_from);
		unset($this->age_to);
		unset($this->search_strict);
		unset($this->page);
	}
		
	private function queryRESTAPIUser(string $username, string $email, string $firstname, string $lastname, string $sex, int $age_from, int $age_to, string $search_strict, int $page) : void
	{
		parent::queryUser
		(
		$username,
		$email,
		$firstname,
		$lastname,
		$sex,
		$age_from,
		$age_to,
		1, // $activated 
		$search_strict,
		$page,
		true
		);
	}
	private function isSetInJSON($json_decoded_array) : bool
	{
		$is_true = false;
			
		if (isset($json_decoded_array['search']['user']['username'])) {
			$this->username = $json_decoded_array['search']['user']['username'];
			$is_true = true;
		} else {
			$this->username = "";
		}
		if (isset($json_decoded_array['search']['user']['email'])) {
			$this->email = $json_decoded_array['search']['user']['email'];
			$is_true = true;
		} else {
		$this->email = "";
		}
		if (isset($json_decoded_array['search']['user']['firstname'])) {
			$this->firstname = $json_decoded_array['search']['user']['firstname'];
			$is_true = true;
		} else {
			$this->firstname = "";
		}
		if (isset($json_decoded_array['search']['user']['lastname'])) {
			$this->lastname = $json_decoded_array['search']['user']['lastname'];
			$is_true = true;
		} else {
			$this->lastname = "";
		}
		if (isset($json_decoded_array['search']['user']['sex'])) {
			$this->sex = $json_decoded_array['search']['user']['sex'];
			$is_true = true;
		} else {
			$this->sex = "";
		}
		if (isset($json_decoded_array['search']['user']['age_from'])) {
			$this->age_from = $json_decoded_array['search']['user']['age_from'];
			$is_true = true;
		} else {
			$this->age_from = 18;
		}
		if (isset($json_decoded_array['search']['user']['age_to'])) {
			$this->age_to = $json_decoded_array['search']['user']['age_to'];
			$is_true = true;
		} else {
			$this->age_to = PHP_INT_MAX;
		}
		if (isset($json_decoded_array['search']['user']['search_strict'])) {
			$this->search_strict = $json_decoded_array['search']['user']['search_strict'];
		} else {
			$this->search_strict = false;
		}
		if (isset($json_decoded_array['search']['user']['page'])) {
			$this->page = $json_decoded_array['search']['user']['page'];
			$is_true = true;
		} else {
			$this->page = 1;
		}
		
		//restrict search
		if (isset($json_decoded_array['search']['query'])) 
		{
			$is_true = false;
		}
		
		return $is_true;
	}
	private function isSetInREQUEST() : bool
	{
		$is_true = false;
			
		if (isset($_REQUEST['username'])) {
			$this->username = $_REQUEST['username'];
			$is_true = true;
		} else {
			$this->username = "";
		}
		if (isset($_REQUEST['email'])) {
			$this->email = $_REQUEST['email'];
			$is_true = true;
		} else {
		$this->email = "";
		}
		if (isset($_REQUEST['firstname'])) {
			$this->firstname = $_REQUEST['firstname'];
			$is_true = true;
		} else {
			$this->firstname = "";
		}
		if (isset($_REQUEST['lastname'])) {
			$this->lastname = $_REQUEST['lastname'];
			$is_true = true;
		} else {
			$this->lastname = "";
		}
		if (isset($_REQUEST['sex'])) {
			$this->sex = $_REQUEST['sex'];
			$is_true = true;
		} else {
			$this->sex = "";
		}
		if (isset($_REQUEST['age_from'])) {
			$this->age_from = $_REQUEST['age_from'];
			$is_true = true;
		} else {
			$this->age_from = 18;
		}
		if (isset($_REQUEST['age_to'])) {
			$this->age_to = $_REQUEST['age_to'];
			$is_true = true;
		} else {
			$this->age_to = PHP_INT_MAX;
		}
		if (isset($_REQUEST['search_strict'])) {
			$this->search_strict = $_REQUEST['search_strict'];
		} else {
			$this->search_strict = "off";
		}
		if (isset($_REQUEST['page'])) {
			$this->page = $_REQUEST['page'];
			$is_true = true;
		} else {
			$this->page = 1;
		}
		
		//restrict search
		if (isset($_REQUEST['q'])) 
		{
			$is_true = false;
		}	
		
		return $is_true;
	}
}


?>