<?php
//TODO IMPLEMENT GEOLOCATION BASED SQL QUERY JOIN ON user_last_location TO user TABLE

class UserQuery
{
	private $db_connection;
	private $row_limit;
	private $number_of_pages;
	
    public $response = array
    (
        "error" => array(),
        "caution" => array(),
        "good" => array(),
		
		
        "search" => array("user" => array())
    );
	
	public function __construct()
    {
        if (isset($_REQUEST["search_user"]))
        {
			if (isset($_REQUEST['active']))
			{ 
				$activated = $_REQUEST['active']; 
			} 
			else 
			{ 
				$activated = 1;
			}
			
            $this->queryUser
            (
                $_REQUEST['username'],
                $_REQUEST['email'],
                $_REQUEST['firstname'],
                $_REQUEST['lastname'],
                $_REQUEST['sex'],
                intval($_REQUEST['age_from']),
                intval($_REQUEST['age_to']),
				intval($activated),
				$_REQUEST['search_strict'],
				intval($_REQUEST['page'])
            );
        }
    }
	
	public function __destruct()
	{
		unset($this->db_connection);
		unset($this->row_limit);
		unset($this->number_of_pages);
		unset($this->response);
	}

	protected function queryUser(string $username, string $email, string $firstname, string $lastname, string $sex, int $age_from, int $age_to, int $activated, string $search_strict, int $page, bool $is_rest = false) : void
	{
		$this->row_limit = 50;
		
		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
		if (!$this->db_connection->connect_errno) 
		{
			$username = $this->db_connection->real_escape_string(strip_tags($username, ENT_QUOTES));
			$email = $this->db_connection->real_escape_string(strtolower(strip_tags($email, ENT_QUOTES)));
			
			$month = date("m");
			$day = date("d");
			$year = date("Y");
			
			$from_year = ($year - $age_from);
			$to_year = ($year - $age_to);
			
			$age_to_query = $to_year."-".$month."-".$day;
			$age_from_query = $from_year."-".$month."-".$day;
			
			$abs_page = abs($page);
			
			if($abs_page > 1)
			{
				$page_offset = $abs_page * $this->row_limit;	
			}
			else
			{
				$page_offset = 0;
			}
			
			if($search_strict == "off")//SPECIAL CASE SEARCH
			{
				if($sex == "male")
				{
					/******************************************************************************/
					$sql = "SELECT id, username, email, firstname, lastname, date_of_birth, sex
							FROM user
							WHERE email = '" .$email. "'
							OR username = '" .$username. "'
							OR firstname = '" .$firstname. "'
							OR lastname = '" .$lastname. "'
							OR sex = 'male'
							OR sex = 'androgynous_male' OR sex = 'intersex_male' OR sex = 'female_to_male'
							OR date_of_birth >= '" .$age_from_query. "' AND date_of_birth <= '" .$age_to_query. "'
							AND activated = '" .$activated. "'
							ORDER BY signup_date DESC;

							;";
					//OR date_of_birth BETWEEN '".$age_to."' AND '"$age_from"'					
					$check_pages = $this->db_connection->query($sql);		
					/******************************************************************************/
					$sql = "SELECT id, username, email, firstname, lastname, date_of_birth, sex
							FROM user
							WHERE email = '" .$email. "'
							OR username = '" .$username. "'
							OR firstname = '" .$firstname. "'
							OR lastname = '" .$lastname. "'
							OR sex = 'male'
							OR sex = 'androgynous_male' OR sex = 'intersex_male' OR sex = 'female_to_male'
							OR date_of_birth >= '" .$age_from_query. "' AND date_of_birth <= '" .$age_to_query. "'
							AND activated = '" .$activated. "'
							ORDER BY signup_date DESC
							LIMIT ".$this->row_limit." OFFSET " .$page_offset. ";

							;";
				}	

				if($sex == "female")
				{
					/******************************************************************************/
					$sql = "SELECT id, username, email, firstname, lastname, date_of_birth, sex
							FROM user
							WHERE email = '" .$email. "'
							OR username = '" .$username. "'
							OR firstname = '" .$firstname. "'
							OR lastname = '" .$lastname. "'
							OR sex = 'female'
							OR sex = 'androgynous_female' OR sex = 'intersex_female' OR sex = 'male_to_female'
							OR date_of_birth >= '" .$age_from_query. "' AND date_of_birth <= '" .$age_to_query. "'
							AND activated = '" .$activated. "'
							ORDER BY signup_date DESC;

							;";
					//OR date_of_birth BETWEEN '".$age_to."' AND '"$age_from"'					
					$check_pages = $this->db_connection->query($sql);		
					/******************************************************************************/
					$sql = "SELECT id, username, email, firstname, lastname, date_of_birth, sex
							FROM user
							WHERE email = '" .$email. "'
							OR username = '" .$username. "'
							OR firstname = '" .$firstname. "'
							OR lastname = '" .$lastname. "'
							OR sex = 'female'
							OR sex = 'androgynous_female' OR sex = 'intersex_female' OR sex = 'male_to_female'
							OR date_of_birth >= '" .$age_from_query. "' AND date_of_birth <= '" .$age_to_query. "'
							AND activated = '" .$activated. "'
							ORDER BY signup_date DESC
							LIMIT ".$this->row_limit." OFFSET " .$page_offset. ";

							;";
				}
			}
			else
			{
			/******************************************************************************/
			$sql = "SELECT id, username, email, firstname, lastname, date_of_birth, sex
					FROM user
					WHERE email = '" .$email. "'
					OR username = '" .$username. "'
					OR firstname = '" .$firstname. "'
					OR lastname = '" .$lastname. "'
					OR sex = '" .$sex. "'
					OR date_of_birth >= '" .$age_from_query. "' AND date_of_birth <= '" .$age_to_query. "'
					AND activated = '" .$activated. "'
					ORDER BY signup_date DESC;

					;";
			//OR date_of_birth BETWEEN '".$age_to."' AND '"$age_from"'					
			$check_pages = $this->db_connection->query($sql);		
			/******************************************************************************/
			$sql = "SELECT id, username, email, firstname, lastname, date_of_birth, sex
					FROM user
					WHERE email = '" .$email. "'
					OR username = '" .$username. "'
					OR firstname = '" .$firstname. "'
					OR lastname = '" .$lastname. "'
					OR sex = '" .$sex. "'
					OR date_of_birth >= '" .$age_from_query. "' AND date_of_birth <= '" .$age_to_query. "'
					AND activated = '" .$activated. "'
					ORDER BY signup_date DESC
					LIMIT ".$this->row_limit." OFFSET " .$page_offset. ";

					;";
			}		
			//OR date_of_birth BETWEEN '".$age_to."' AND '"$age_from"'
			$check_user = $this->db_connection->query($sql);
			if ($check_user->num_rows > 0)
			{
				$this->number_of_pages = (floor($check_pages->num_rows/$this->row_limit));
				if($this->number_of_pages < 0)
				{
					$this->number_of_pages = 0;
				}
				
				
			    $index = 0;
				
				$this->response['search']['pages'] = $this->number_of_pages;
				$this->response['search']['current_page'] = $abs_page;
				foreach ( $check_user as  $response_row)
				{
                    $this->response['search']['user'][$index]['id'] = $response_row["id"];
                    $this->response['search']['user'][$index]['username'] = $response_row["username"];
                    $this->response['search']['user'][$index]['email'] = $response_row["email"];
                    $this->response['search']['user'][$index]['firstname'] = $response_row["firstname"];
                    $this->response['search']['user'][$index]['lastname'] = $response_row["lastname"];
                    $this->response['search']['user'][$index]['dateofbirth'] = $response_row["date_of_birth"];
                    $this->response['search']['user'][$index]['age'] =  date_create($response_row["date_of_birth"])->diff(date_create('today'))->y;
                    $this->response['search']['user'][$index]['sex'] = $response_row["sex"];

                    $index++;
				}
			}
		} 
	}
	
	public function getNumberOfPages() : int
	{
		if (isset($this->number_of_pages))
		{
			return $this->number_of_pages;			
		}
		else
		{
			return 0;
		}

	}
}

?>