<?php
class UserSearch
{
	private $db_connection;
	private $row_limit;
	private $number_of_pages;
	
    public $response = array
    (
        "error" => array(),
        "caution" => array(),
        "good" => array(),
		
		
        "search" => array("user" => array())
    );
	
	public function __construct() 
	{
		if (isset($_REQUEST["q"]))
		{
			$this->searchUser
			(
				$_REQUEST["q"],
				intval($_REQUEST['page'])
			);
		}
	}
	
	protected function searchUser(string $search, int $page, bool $is_rest = false)
	{

			$this->row_limit = 50;
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			
			$this->db_connection->set_charset("utf8");
			if (!$this->db_connection->set_charset("utf8")) 
			{ 
				$this->response["error"] = array(
								'message' => " I'm a teapot (RFC 2324), ".$this->db_connection->error." ",
								'code' => 418
								);
			}
			
			if (!$this->db_connection->connect_errno) 
			{
				$username = $this->db_connection->real_escape_string($search);
				$sql = "SELECT id, username, email, firstname, lastname, sex
						FROM user
						WHERE username LIKE '" .$search. "' 
						OR email LIKE '" .$search. "' 
						OR firstname LIKE '" .$search. "' 
						OR lastname LIKE '" .$search. "'
						;";
                $check_pages = $this->db_connection->query($sql);
				
				$this->number_of_pages = (floor($check_pages->num_rows/$this->row_limit));
				if($this->number_of_pages < 0)
				{
					$this->number_of_pages = 0;
				}

				$abs_page = abs($page);
				
				if($abs_page > 1)
				{
					$page_offset = $abs_page * $this->row_limit;	
				}
				else
				{
					$page_offset = 0;
				}
			

				$sql = "SELECT id, username, email, firstname, lastname, sex
						FROM user
						WHERE username LIKE '" .$search. "' 
						OR email LIKE '" .$search. "' 
						OR firstname LIKE '" .$search. "' 
						OR lastname LIKE '" .$search. "'
						ORDER BY signup_date DESC
						LIMIT ".$this->row_limit." OFFSET " .$page_offset. ";
						;";
						
                $search_check = $this->db_connection->query($sql);
				
				if ($search_check->num_rows > 0)
				{
					$index = 0;
					$this->response['search']['pages'] = $this->number_of_pages;
					$this->response['search']['current_page'] = $abs_page;
					foreach ( $search_check as  $response_row)
					{
						$this->response['search']['user'][$index]['id'] = $response_row["id"];
						$this->response['search']['user'][$index]['username'] = $response_row["username"];
						$this->response['search']['user'][$index]['email'] = $response_row["email"];
						$this->response['search']['user'][$index]['firstname'] = $response_row["firstname"];
						$this->response['search']['user'][$index]['lastname'] = $response_row["lastname"];
						$this->response['search']['user'][$index]['dateofbirth'] = $response_row["date_of_birth"];
						$this->response['search']['user'][$index]['age'] =  date_create($response_row["date_of_birth"])->diff(date_create('today'))->y;
						$this->response['search']['user'][$index]['sex'] = $response_row["sex"];

						$index++;
					}
				}
				else 
				{ 
					$this->response["success"] = false;
					$this->response["error"] = array(
									'message' => 'Not Found, This user does not exist.',
									'code' => 404
									);
				}
			} 
			else 
			{ 
				$this->response["success"] = false;
				$this->response["error"] = array(
								'message' => " I'm a teapot (RFC 2324), Looks like a Database connection problem.",
								'code' => 418
								);
			}
	}
	
	
	public function getNumberOfPages() : int
	{
		if (isset($this->number_of_pages))
		{
			return $this->number_of_pages;			
		}
		else
		{
			return 0;
		}

	}
}	
?>