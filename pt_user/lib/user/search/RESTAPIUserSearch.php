<?php
include_once("UserSearch.php");

class RESTAPIUserSearch extends UserSearch
{
	
	private $query;
	private $page;
	
	public function __construct($json_decoded_array) 
	{
		if ($this->isSetInJSON($json_decoded_array))
		{
			$this->searchRESTAPIUser
			(
				$this->query,
				intval($this->page)
			);
		}
		if ($this->isSetInREQUEST())
		{
			$this->searchRESTAPIUser
			(
				$this->query,
				intval($this->page)
			);
		}
	}
	
	private function searchRESTAPIUser(string $search, int $page)
	{
		parent::searchUser
		(
			$search,
			$page,
			true
		);
	}
	private function isSetInJSON($json_decoded_array) : bool
	{
		$is_true = false;
			
		if (isset($json_decoded_array['search']['query'])) {
			$this->query = $json_decoded_array['search']['query'];
			$is_true = true;
		}
		if (isset($json_decoded_array['search']['page'])) {
			$this->page = $json_decoded_array['search']['page'];
		} else {
			$this->page = 1;
		}

		return $is_true;
	}
	private function isSetInREQUEST() : bool
	{
		$is_true = false;
			
		if (isset($_REQUEST['q'])) {
			$this->query = $_REQUEST['q'];
			$is_true = true;
		}

		if (isset($_REQUEST['page'])) {
			$this->page = $_REQUEST['page'];
		} else {
			$this->page = 1;
		}
			
		return $is_true;
	}
}	
?>