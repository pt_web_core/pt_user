<?php

class Account
{
	private $db_connection;
	
	public $error = array();
	public $caution = array();
	public $good = array();
	public $response = array();
	
	public function __construct() 
	{ 
		
	}
	
	public function requestRecovery()
	{
		if (isset($_POST["recover"])) 
		{
			if (empty($_POST['username_or_email'])) { $this->error[] = "Username field was empty."; } 
			elseif (!empty($_POST['username_or_email']))
			{
				$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
				if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
				if (!$this->db_connection->connect_errno) 
				{
					$username_or_email = $this->db_connection->real_escape_string($_POST['username_or_email']);
					$sql = "SELECT username, password_change_date, email, activated, activation_key
							FROM user
							WHERE username = '".$username_or_email."' 
							OR email = '".$username_or_email."';";
					$recovery_check = $this->db_connection->query($sql);
					if ($recovery_check->num_rows == 1) 
					{
						$response_row = $recovery_check->fetch_object();
						if (($_POST['username_or_email'] == $response_row->email) && ($response_row->activated == 1))
						{
							$password_change_date = $response_row->password_change_date;
							$remove_date_delimiter = str_replace("-", "", $password_change_date);
							$remove_time_delimiter = str_replace(":", "", $remove_date_delimiter);
							$remove_space_delimiter = str_replace(" ", "", $remove_time_delimiter);
							$recovery_ticket = $remove_space_delimiter;
							
							$this->response['email'] = $response_row->email;
							$this->response['recovery_ticket'] = $recovery_ticket;
							$this->response['activation_key'] = $response_row->activation_key;
							
							$_SESSION['recovery_message_status'] = 1;
						} 
						else { $this->error[] = "Wrong username or email. Try again."; }
					}
					else { $this->error[] = "This user does not exist."; }
				} 
				else { $this->error[] = "Database connection problem."; }
			}
		}
	}
	public function isRecoveryRequested() 
	{
		if (isset($_SESSION['recovery_message_status']) AND $_SESSION['recovery_message_status'] == 1) 
		{
			return true;
		}  
		return false; 
	}
	
	
	public function recoverAccount()
	{
		if ((isset($_GET["email"])) && (isset($_GET["recovery_ticket"])) && (isset($_GET["activation_key"])))
		{
			session_name('persistence');
			session_start();
			
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
			if (!$this->db_connection->connect_errno) 
			{
				$email = $this->db_connection->real_escape_string($_GET['email']);
				$recovery_ticket = $_GET["recovery_ticket"];
				$activation_key = $_GET["activation_key"];
						
				$sql = "SELECT id, password_change_date, email, activated, activation_key
						FROM user
						WHERE email = '" .$email. "'
						AND activation_key = '" .$activation_key. "'
						;";
				$Activate_check = $this->db_connection->query($sql);
				if ($Activate_check->num_rows == 1) 
				{
					$response_row = $Activate_check->fetch_object();
					if (($_GET['email'] == $response_row->email) && ($_GET['activation_key'] == $response_row->activation_key) && ($response_row->activated == 1))
					{
						$password_change_date= $response_row->password_change_date;
						
						$remove_date_delimiter = str_replace("-", "", $password_change_date);
						$remove_time_delimiter = str_replace(":", "", $remove_date_delimiter);
						$remove_space_delimiter = str_replace(" ", "", $remove_time_delimiter);
						
						$recovery_ticket_check = $remove_space_delimiter;
						
						if ($recovery_ticket_check == $recovery_ticket) 
						{ 
							$_SESSION['id'] = $response_row->id;
							$_SESSION['Recovery_status'] = 1; 
						}
						
					} 
					elseif ($response_row->activated == 0){ $this->caution[] = "This account is inactive, please try to activate this account before trying to change your password." ;}
				}
				else { $this->error[] = "This email or activation key does not exist."; }
			} 
			else { $this->error[] = "Database connection problem."; }
		}
	}
	
	public function isAccountRecovered() 
	{ 
		if (isset($_SESSION['Recovery_status']) AND $_SESSION['Recovery_status'] == 1) 
		{ 
			return true; 
		}  
		return false; 
	}
	
	public function activateAccount()
	{
		if ((isset($_GET["email"])) && (isset($_GET["activation_key"])))
		{
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
			if (!$this->db_connection->connect_errno) 
			{
				$email = $this->db_connection->real_escape_string($_GET['email']);
				$sql = "SELECT id, email, activated, activation_key
						FROM user
						WHERE email = '" .$email. "';";
				$Activate_check = $this->db_connection->query($sql);
				if ($Activate_check->num_rows == 1) 
				{
					$response_row = $Activate_check->fetch_object();
					if (($_GET['email'] == $response_row->email) && ($_GET['activation_key'] == $response_row->activation_key))
					{
						$activated = true; // 1 is True
						$sql = "UPDATE user
								SET activated='". $activated . "'
								WHERE email = '" .$email. "';";
						

						$id = $response_row->id;
						$activation_type = 1; // 1 for email activation
						
						$query_new_activation = $this->db_connection->query($sql);
						if ($query_new_activation) 
						{

							$sql = "INSERT INTO user_activation_type (id, activation_type)
							VALUES('" . $id . "', '" . $activation_type . "');";
							$this->db_connection->query($sql);								

							$_SESSION['Activation_status'] = 1;
						}
					} 
					elseif ($response_row->activated == 1){ $_SESSION['Activation_status'] = 1; }
					else { $this->error[] = "Email and Activation Key dose not match."; }
				}
				else { $this->error[] = "This email does not exist."; }
			} 
			else { $this->error[] = "Database connection problem."; }
		}
	}
	public function isAccountActivated() 
	{
		if (isset($_SESSION['Activation_status']) && $_SESSION['Activation_status'] == 1) 
		{
			return true;
		}  
		return false;
	}
	
	public function deactivateAccount()
	{
		if (isset($_POST["deactivate"]))
		{
			if (empty($_POST['username'])) { $this->error[] = "Username field was empty."; } 
			elseif (empty($_POST['password'])) { $this->error[] = "Password field was empty."; }
			elseif (!empty($_POST['username']) && !empty($_POST['password'])) 
			{
				$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
				if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
				if (!$this->db_connection->connect_errno) 
				{
					$username_or_email = $this->db_connection->real_escape_string($_POST['username']);
					$sql = "SELECT id, username, email, password_hash, activated
							FROM user
							WHERE username = '" .$username_or_email. "' 
							OR email = '" .$username_or_email. "';";
					$deactivate_check = $this->db_connection->query($sql);
					if ($deactivate_check->num_rows == 1) 
					{
						$response_row = $deactivate_check->fetch_object();
						if ((password_verify($_POST['password'], $response_row->password_hash)) && ($response_row->activated == 1)) 
						{
							$id = $response_row->id;
							$activated = 0; // 0 is Deactivate
							$sql = "UPDATE user
									SET activated='". $activated . "'
									WHERE username = '" .$username_or_email. "' 
									OR email = '" .$username_or_email. "';";
							
							$query_new_deactivation = $this->db_connection->query($sql);
							$_SESSION['Deactivate_status'] = 1;
							
														
							$sql = "DELETE FROM user_activation_type
							WHERE id = ('" . $id . "');";
							$this->db_connection->query($sql);
							
						} 
						else { $this->error[] = "Wrong password. Try again."; }
					}
					else { $this->error[] = "This user does not exist."; }
				} 
				else { $this->error[] = "Database connection problem."; }
			}
		}
	}
   public function isAccountDeactivated() 
   {
	   if (isset($_SESSION['Deactivate_status']) AND $_SESSION['Deactivate_status'] == 1)
		{
		   return true;
		}  
		return false; 
   }
}

?>