<?php

class Password
{
	private $db_connection;
	public $response = array
    (
        "error"  => array(),
        "caution"  => array(),
        "good"  => array(),
    );

	public function __construct() 
	{ 

	}
	
	public function __destruct()
	{
		unset($db_connection);
		unset($response);
	}

	public function createNewPassword() : void
	{
		if ( (isset($_SESSION['id'])) && (isset($_POST["create_password"])) ) 
		{
			if (empty($_POST['password']) || empty($_POST['confirm_password'])) { $this->response['error'][] = "Empty Password"; } 
			elseif ($_POST['password'] !== $_POST['confirm_password']) { $this->response['error'][] = "Password and confirm password repeat are not the same"; } 
			elseif (strlen($_POST['password']) < 6) { $this->response['error'][] = "Password has a minimum length of 6 characters"; } 

			elseif 
			(
				   !empty($_POST['password'])
				&& !empty($_POST['confirm_password'])
				&& ($_POST['password'] === $_POST['confirm_password'])

			) 
			
			{
				$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
				if (!$this->db_connection->set_charset("utf8")) { $this->response['error'][] = $this->db_connection->error; }
				if (!$this->db_connection->connect_errno) 
				{
					$password = $_POST['password'];
					$password_hash = password_hash($password, PASSWORD_HASHING);
					$password_change_date = date("Y-m-d H:i:s");
					
					$id = $_SESSION['id'];

					$sql = "UPDATE user
							SET password_hash='". $password_hash . "',  password_change_date='". $password_change_date . "'
							WHERE id ='".$id. "';";
					$query_new_password_hash = $this->db_connection->query($sql);
					if ($query_new_password_hash) { $_SESSION['password_set'] = 1; }
					else { $this->response['error'][] = "Sorry, your password change has failed. Please try again."; }
					
					$this->db_connection->close();
				} 
				else { $this->response['error'][] = "Sorry, no database connection."; }
			} 
			else { $this->response['error'][] = "An unknown error occurred."; }
		}
	}
	public function isPasswordSet() : bool
	{
		if (isset($_SESSION['password_set']) AND $_SESSION['password_set'] == 1)
		{
			return true;
		}
		return false;
	}
}

?>