<?php

include_once("Login.php");

class RESTAPILogin extends Login
{
    function __construct($json_decoded_array)
    {
        // JSON
        if($this->isSetInJSON($json_decoded_array))
        {
            $this->checkRESTAPILogin
            (
                $json_decoded_array["user"]["username"],
                $json_decoded_array["user"]["password"]
            );
        }
        // URL POST OR COOKIE
        if($this->isSetInREQUEST())
        {
            $this->checkRESTAPILogin
            (
                $_REQUEST['username'],
                $_REQUEST['password']
            );
        }
    }
	
	public function __destruct()
	{
		parent::__destruct();
	}
	
    private function checkRESTAPILogin($username, $password) :void
    {
        parent::checkLogin($username, $password, true);
    }

    private function isSetInJSON($json_decoded_array)  : bool
    {
        if
        (
            isset($json_decoded_array["user"]["username"]) &&
            isset($json_decoded_array["user"]["password"])
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private function isSetInREQUEST() : bool
    {
        if
        (
            isset($_REQUEST["username"]) &&
            isset($_REQUEST["password"])
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
	public static function isRESTAPIUserLoggedIn() : bool
	{
		if(isset($_SESSION['id']))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}