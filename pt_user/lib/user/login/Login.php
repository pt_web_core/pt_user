<?php
class Login
{
	private $db_connection;
    public $response = array
    (
        "error"  => array(),
        "caution"  => array(),
        "good"  => array(),

        "user"  => array()
    );
	
	public function __construct()
    {
        $this->startSession();
		if (isset($_POST["login"]))
		{
			$this->checkLogin
			(
				$_POST['username_or_email'],
				$_POST['password']
			);
		}
	}
	
	public function __destruct()
	{
		unset($db_connection);
		unset($response);
	}
	
	protected function checkLogin(string $username_or_email, string $password, bool $is_rest = false) : void
	{

		if (empty($username_or_email))
		{
		    $this->response['error'][] = "Username field was empty.";
		}
		elseif (empty($password))
        {
            $this->response['error'][] = "Password field was empty.";
        }
		elseif (!empty($username_or_email) && !empty($password))
		{
            if($is_rest == false)
            {
                if (isset($_POST['keep_me_logged_in']) && ($_POST['keep_me_logged_in'] == 'on'))
                {
                    $_SESSION['keep_me_logged_in'] = Yes;
                    setcookie("kmli", "Yes", 0, '/');
                } elseif (!isset($_POST['keep_me_logged_in']))
                {
                    $_SESSION['keep_me_logged_in'] = No;
                    setcookie("kmli", "No", time() + (60 * 5), '/');
                }
            }
			
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if (!$this->db_connection->set_charset("utf8"))
			{
                $this->response['error'][] = $this->db_connection->error;
			}
			if (!$this->db_connection->connect_errno) 
			{
				$username_or_email = $this->db_connection->real_escape_string($username_or_email);
				$sql = "SELECT id, username, password_hash, email, firstname, lastname, date_of_birth, sex, activated, activation_key
						FROM user
						WHERE username = '" .$username_or_email. "' 
						OR email = '" .$username_or_email. "';";
				$login_check = $this->db_connection->query($sql);
				if ($login_check->num_rows == 1) 
				{
					$response_row = $login_check->fetch_object();
					if (password_verify($password, $response_row->password_hash))
					{
					    if($is_rest == false)
					    {
                            $_SESSION['id'] =  $response_row->id;
                            $_SESSION['username'] = $response_row->username;
                            $_SESSION['email'] = $response_row->email;
                            $_SESSION['login_status'] = 1;
                        }
                        if($is_rest == true)
                        {
                            $this->response["user"]["id"] = $response_row->id;
                            $this->response["user"]["username"] = $response_row->username;
                            $this->response["user"]["email"] = $response_row->email;
                            $this->response["user"]["firstname"] = $response_row->firstname;
                            $this->response["user"]["lastname"] = $response_row->lastname;

                            $this->response["user"]["date_of_birth"] = date('c', strtotime($response_row->date_of_birth)); //DATE_ISO8601

                            $this->response["user"]["sex"] = $response_row->sex;
                            $this->response["user"]["activation_key"] = $response_row->activation_key;

                            $this->response["login"]["success"] = true;

                            $activated = 1; // activation_key set to 2 is True but activated from API in user_activation_type table
                            if ($response_row->activated != $activated)
                            {
                                $id = $response_row->id;

                                $sql = "UPDATE user
								SET activated='". $activated . "'
								WHERE id = '" .$id. "';";

                                $this->db_connection->query($sql);
                            }
                            $this->response["success"] = array(
                                'message' => 'Success',
                                'code' => 200
                            );
                        }
                        if( $is_rest == true && !password_verify($password, $response_row->password_hash) )
                        {
                            $this->response["login"]["success"] = false;
                            $this->response["error"] = array(
                                'message' => 'Unauthorized, Wrong password. Try again.',
                                'code' => 401
                            );
                        }

					} 
					else { $this->response['error'][] = "Wrong password. Try again."; }
				}
				else { $this->response['error'][] = "This user does not exist."; }
			} 
			else { $this->response['error'][] = "Database connection problem."; }
			$this->db_connection->close();
		}
	}
	public function Logout() : void
    {
        $_SESSION = array();
        session_destroy();
        setcookie("kmli", "", time() - (1), '/');
        $this->response['good'][] = "You have been logged out.";
    }
	public function isUserLoggedIn() : bool
    {
        if (isset($_SESSION['login_status']) && $_SESSION['login_status'] == 1)
        {
            return true;
        }
        return false;
    }

    public function startSession() : void
    {
        if(!isset($_SESSION))
        {
            session_name('persistence');
            session_start();
        }
    }
}

?>