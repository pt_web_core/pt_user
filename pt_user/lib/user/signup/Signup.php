<?php

class Signup
{
	private $db_connection;

	public $response = array
    (
        "error" => array(),
        "caution" => array(),
        "good" => array(),

        "user" => array()
    );

	public function __construct()
    {
        if (isset($_POST["signup"]))
        {
            $this->checkSignup
            (
                $_POST['username'],
                $_POST['email'] ,
                "",                     //Pasword is not set in POST but for RESTAPI!
                $_POST['firstname'],
                $_POST['lastname'],
                $_POST['month'],
                $_POST['day'],
                $_POST['year'],
                $_POST['sex']
            );
        }
    }
	
	public function __destruct()
	{
		unset($db_connection);
		unset($response);
	}
	
	protected function checkSignup(string $username, string $email , string $password, string $firstname, string $lastname, int $date_of_birth_month, int $date_of_birth_day, int $date_of_birth_year, string $sex, bool $is_rest = false) : void
	{
		if (empty($username)) { $this->response['error'][] = "Empty Username"; }
		
		elseif (strlen($username) > 64 || strlen($username) < 2) { $this->response['error'][] = "Username cannot be shorter than 2 or longer than 64 characters"; }
		elseif (!preg_match('/^[a-z\d]{2,64}$/i', $username)) { $this->response['error'][] = "Username does not fit the name scheme: only a-Z and numbers are allowed, 2 to 64 characters"; }
		
		elseif (empty($email)) { $this->response['error'][] = "Email cannot be empty"; }
		elseif (strlen($email) > 64) { $this->response['error'][] = "Email cannot be longer than 64 characters"; }
		elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) { $this->response['error'][] = "Your email address is not in a valid email format"; }
		
		elseif (empty($firstname)) { $this->response['error'][] = "Firstname cannot be empty"; }
		elseif (empty($lastname)) { $this->response['error'][] = "Lastname cannot be empty"; }
		
		elseif (empty($date_of_birth_month)) { $this->response['error'][] = "Month cannot be empty"; }
		elseif (empty($date_of_birth_day)) { $this->response['error'][] = "Day cannot be empty"; }
		elseif (empty($date_of_birth_year)) { $this->response['error'][] = "Year cannot be empty"; }
		
		elseif (!empty($date_of_birth_month== 2) && !empty($date_of_birth_day > 28)) { $this->response['error'][] = "February only has 28 days."; }
		elseif (!empty($date_of_birth_month== 4) && !empty($date_of_birth_day > 30)) { $this->response['error'][] = "April only has 30 days."; }
		elseif (!empty($date_of_birth_month== 6) && !empty($date_of_birth_day > 30)) { $this->response['error'][] = "June only has 30 days."; }
		elseif (!empty($date_of_birth_month== 9) && !empty($date_of_birth_day > 30)) { $this->response['error'][] = "September only has 30 days."; }
		elseif (!empty($date_of_birth_month== 11) && !empty($date_of_birth_day > 30)) { $this->response['error'][] = "November only has 30 days."; }

		elseif (empty($sex)) { $this->response['error'][] = "Sex cannot be empty"; }
		
		elseif 
		(
			   !empty($username)
			&& strlen($username) <= 64
			&& strlen($username) >= 2
			&& preg_match('/^[a-z\d]{2,64}$/i', $username)
			
			&& !empty($email)
			&& strlen($email) <= 64
			&& filter_var($email, FILTER_VALIDATE_EMAIL)
			
			&& !empty($firstname)
			&& !empty($lastname)
			
			&& !empty($date_of_birth_month)
			&& !empty($date_of_birth_day)
			&& !empty($date_of_birth_year)
			
			&& !empty($sex== 'male') || !empty($sex== 'female') 
			|| !empty($sex== 'intersex_male') || !empty($sex== 'intersex_female')
			|| !empty($sex== 'male_to_female') || !empty($sex== 'female_to_male')
			|| !empty($sex== 'androgynous_male') || !empty($sex== 'androgynous_female')
			
		) 
		
		{
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if (!$this->db_connection->set_charset("utf8")) { $this->response['error'][] = $this->db_connection->error; }
			if (!$this->db_connection->connect_errno) 
			{
				$username = $this->db_connection->real_escape_string(strip_tags($username, ENT_QUOTES));
				$_SESSION['username'] = $username;
				$email = $this->db_connection->real_escape_string(strtolower(strip_tags($email, ENT_QUOTES)));
				$_SESSION['email'] = $email;

				if($is_rest == false)
                {
                    $password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 14 );
                    $_SESSION['password_random']= $password;
                }
				$password_hash = password_hash($password, PASSWORD_HASHING);
				
				$firstname = $this->db_connection->real_escape_string(strip_tags($firstname, ENT_QUOTES));//Sanitize
				$lastname = $this->db_connection->real_escape_string(strip_tags($lastname, ENT_QUOTES));
				
				$month = $date_of_birth_month;
				$day = $date_of_birth_day;
				$year = $date_of_birth_year;
				$date_of_birth = $year."-".$month."-".$day;
				
				$sex = $sex;
				if($is_rest == false)
                {
                    $activated = 0;
                }

                if($is_rest == true)
                {
                    $activated = 1;
                    $activation_type = 2; // 2 is API SIGNNUP TRUE
                }

				$signup_date = date("Y-m-d H:i:s");
				
				$sql = "SELECT * FROM user WHERE username = '" . $username . "' OR email = '" . $email . "';";
				$query_check_username = $this->db_connection->query($sql);
				if ($query_check_username->num_rows == 1) { $this->response['error'][] = "Sorry, that username or email address is already taken."; }
				else 
				{
					$sql = "INSERT INTO user (username, password_hash, email, firstname, lastname, date_of_birth, sex, activated, signup_date)
							VALUES('" . $username . "', '" . $password_hash . "', '" . $email . "', '" . $firstname . "', '" . $lastname . "', '" . $date_of_birth . "', '" . $sex . "', '" . $activated . "', '" . $signup_date . "');";
					$this->db_connection->query($sql);
					
					if ($is_rest == false)
					{
					    $_SESSION['signup_status'] = 1;
					}
                    else if ($is_rest == true)
                    {
                        //OUTPUT USER JSON OBJECT
                        $sql = "SELECT * FROM user 
                                WHERE username = '" . $username . "' 
                                AND email = '" . $email . "';";

                        $query_get_user = $this->db_connection->query($sql);
                        if ($query_get_user->num_rows == 1)
                        {
                            $response_row = $query_get_user->fetch_object();
							
							//CREATE ACTIVATION KEY
							$unique = SITE_UNIQUE_KEY;
							
							$email= $response_row->email;
							$id = $response_row->id;
							
							$secret = sha1(md5($password)) * $unique + md5($id);
							$string = $secret.'=='.$email;
							
							$new_activation_key = sha1($string);
							
							$password_change_date = date("Y-m-d H:i:s");

							$sql = "UPDATE user
									SET activation_key='". $new_activation_key . "', password_change_date='". $password_change_date . "'
									WHERE id ='".$id. "';";
									
							$this->db_connection->query($sql);
							
							
							if($is_rest == true)
							{
								$sql = "INSERT INTO user_activation_type (id, activation_type)
								VALUES('" . $id . "', '" . $activation_type . "');";
								$this->db_connection->query($sql);								
							}

							
							//OUTPUT JSON
                            $this->response["user"]["id"] = $response_row->id;
                            $this->response["user"]["username"] = $response_row->username;
                            $this->response["user"]["email"] = $response_row->email;
                            $this->response["user"]["firstname"] = $response_row->firstname;
                            $this->response["user"]["lastname"] = $response_row->lastname;

                            $this->response["user"]["date_of_birth"] = date('c', strtotime($response_row->date_of_birth)); //DATE_ISO8601

                            $this->response["user"]["sex"] = $response_row->sex;
                            $this->response["user"]["activation_key"] = $new_activation_key;
                        }
                    }
					else
                    {
                        $this->response['error'][] = "Sorry, your registration failed. Please go back and try again.";
                    }
				}
				$this->db_connection->close();
			} 
			else { $this->response['error'][] = "Sorry, no database connection."; }
		} 
		else { $this->response['error'][] = "An unknown error occurred."; }
	}
	
	public function checkSignupID() : void
	{
		if ( (isset($_SESSION['username'])) && (isset($_SESSION['email'])) ) 
		{
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

			if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
			if (!$this->db_connection->connect_errno) 
			{
				$username = $_SESSION['username'];
				$email = $_SESSION['email'];
				$sql = "SELECT id FROM user
						WHERE username = '" .$username. "'  AND email = '" .$email. "';";
				$check_ID = $this->db_connection->query($sql);
				if ($check_ID->num_rows == 1) 
				{
					$response_row = $check_ID->fetch_object();
					$_SESSION['id'] = $response_row->id;
				}
			} 
			$this->db_connection->close();
		}
	}
	
	public function isUserSignedUp() : bool
    {
        if (isset($_SESSION['signup_status']) && $_SESSION['signup_status'] == 1)
        {
            return true;
        }
        return false;
    }
}

?>