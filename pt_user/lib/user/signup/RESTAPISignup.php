<?php
include_once("Signup.php");

class RESTAPISignup extends Signup
{

    function __construct($json_decoded_array)
    {
        // JSON
        if($this->isSetInJSON($json_decoded_array))
        {
            $this->checkRESTAPISignup
            (
                $json_decoded_array["user"]["username"],
                $json_decoded_array["user"]["email"],
                $json_decoded_array["user"]["password"],
                $json_decoded_array["user"]["firstname"],
                $json_decoded_array["user"]["lastname"],
                $json_decoded_array["user"]["date_of_birth"],
                $json_decoded_array["user"]["sex"]
            );

            $this->response["success"] = array(
                'message' => 'Success',
                'code' => 200
            );
        }
        // URL POST OR COOKIE
        if($this->isSetInREQUEST())
        {
            $this->checkRESTAPISignup
            (
                $_REQUEST["username"],
                $_REQUEST["email"],
                $_REQUEST["password"],
                $_REQUEST["firstname"],
                $_REQUEST["lastname"],
                $_REQUEST["date_of_birth"],
                $_REQUEST["sex"]
            );

            $this->response["success"] = array(
                'message' => 'Success',
                'code' => 200
            );
        }
    }
	public function __destruct()
	{
		parent::__destruct();
	}
	
    private function checkRESTAPISignup(string $username, string $email , string $password, string $firstname, string $lastname, string $date_of_birth, string $sex)
    {
        parent::checkSignup
        (
            $username,
            $email,
            $password,
            $firstname,
            $lastname,
            date('m',strtotime($date_of_birth)),
            date('d',strtotime($date_of_birth)),
            date('Y',strtotime($date_of_birth)),
            $sex,
            true
            );
    }

    //Helper Functions
    private function isSetInJSON($json_decoded_array) : bool
    {
        if
        (
            isset($json_decoded_array["user"]["username"]) &&
            isset($json_decoded_array["user"]["email"]) &&
            isset($json_decoded_array["user"]["password"]) &&
            isset($json_decoded_array["user"]["firstname"]) &&
            isset($json_decoded_array["user"]["lastname"]) &&
            isset($json_decoded_array["user"]["date_of_birth"]) &&
            isset($json_decoded_array["user"]["sex"]) 

        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private function isSetInREQUEST() : bool
    {
        if
        (
            isset($_REQUEST["username"]) &&
            isset($_REQUEST["email"]) &&
            isset($_REQUEST["password"]) &&
            isset($_REQUEST["firstname"]) &&
            isset($_REQUEST["lastname"]) &&
            isset($_REQUEST["date_of_birth"]) &&
            isset($_REQUEST["sex"]) 
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
?>