<?php

class ActivationKey
{
	private $db_connection;
	
	public $error = array();
	public $caution = array();
	public $good = array();
	
	public function __construct() 
	{
		
	}

	public function createActivationKey()
	{
		if (isset($_POST["signup"]))
		{
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
			if (!$this->db_connection->connect_errno) 
			{
				$unique = SITE_UNIQUE_KEY;
				
				$password = isset($_SESSION['password_random']);
				$email= $_SESSION['email'];
				$id = $_SESSION['id'];
				
				$secret = sha1(md5($password)) * $unique + md5($id);
				$string = $secret.'=='.$email;
				
				$activation_key = sha1($string);

				$sql = "UPDATE user
						SET activation_key='". $activation_key . "'
						WHERE id ='".$id. "';";
						
				$query_new_activation_key = $this->db_connection->query($sql);
				if ($query_new_activation_key) { $_SESSION['activation_key_set'] = 1; }
				else { $this->error[] = "Sorry, your password change has failed. Please try again."; }
			} 
			else { $this->error[] = "Sorry, no database connection."; }
		}
	}	

	public function createNewActivationKey()
	{
		if ( (isset($_SESSION['email'])) && (isset($_SESSION['id'])) ) 
		{
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
			if (!$this->db_connection->connect_errno) 
			{
				$unique = SITE_UNIQUE_KEY;
				
				$password = isset($_POST['password']);
				$email= $_SESSION['email'];
				$id = $_SESSION['id'];
				
				$secret = sha1(md5($password)) * $unique + md5($id);
				$string = $secret.'=='.$email;
				
				$new_activation_key = sha1($string);

				$sql = "UPDATE user
						SET activation_key='". $new_activation_key . "'
						WHERE id ='".$id. "';";
						
				$query_renew_activation_key = $this->db_connection->query($sql);
				if ($query_renew_activation_key) { $_SESSION['new_activation_key_set'] = 1; }
				else { $this->error[] = "Sorry, your password change has failed. Please try again."; }
			} 
			else { $this->error[] = "Sorry, no database connection."; }
		}
	}

	public function checkActivationKey()
	{
		if (isset($_SESSION['activation_key_set'])) 
		{
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

			if (!$this->db_connection->set_charset("utf8")) { $this->error[] = $this->db_connection->error; }
			if (!$this->db_connection->connect_errno) 
			{
				$sql = "SELECT activation_key
						FROM user
						WHERE email = '" .$_SESSION['email']. "';";
				$check_ID = $this->db_connection->query($sql);
				if ($check_ID->num_rows == 1) 
				{
					$response_row = $check_ID->fetch_object();
					$activation_key = $response_row->activation_key;
					$_SESSION['activation_key'] = $activation_key;
				}
			} 
		}
	}
	
}

?>