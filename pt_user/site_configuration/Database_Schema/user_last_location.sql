
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE TABLE `user_last_location` (
  `id` int(11) DEFAULT NULL COMMENT 'Sync with id from user table',
  `country_code` char(2) COLLATE utf8_bin DEFAULT NULL COMMENT 'Country Code XX',
  `country_name` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'Country Name in English',
  `region_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'State/Province/Region',
  `city_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'City/Town',
  `latitude` double DEFAULT NULL COMMENT 'Latitude Approximation',
  `longitude` double DEFAULT NULL COMMENT 'Longitude Approximation'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Last Location of User Baced on IP/GeoLocation';