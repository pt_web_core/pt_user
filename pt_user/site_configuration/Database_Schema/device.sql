
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `device` 
(
	`id` int(11) DEFAULT NULL COMMENT 'id sync with id from user table',
	`access_token` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Used for Device Session',
	`first_login_date` datetime NOT NULL COMMENT 'Date of First Login',
	`device_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ANDROID_ID, GUID, UUID, ...',
	`device_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Human readable name for user',
	UNIQUE KEY `device_id` (`device_id`),
	UNIQUE KEY `access_token` (`access_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='device authorization data';

