
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `user` 
(
	`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'user id',
	`username` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'used for login',
	`password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'passwords should not be stored as plain text',
	`password_change_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT 'used for record keeping',
	`email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'is private, used for login',
	`firstname` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'used for record keeping',
	`lastname` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'used for record keeping',
	`date_of_birth` date NOT NULL COMMENT 'used for age restriction',
	`sex` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'used for search',
	`signup_date` datetime NOT NULL  COMMENT 'used record keeping',
	`activated` boolean DEFAULT FALSE COMMENT '1 TRUE / 0 FALSE',
	`activation_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'used for special authentication',
	PRIMARY KEY (`id`),
	UNIQUE KEY `username` (`username`),
	UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM	DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data';
