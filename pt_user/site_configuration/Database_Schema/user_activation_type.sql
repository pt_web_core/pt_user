
CREATE TABLE `user_activation_type` 
(
	`id` int(11) DEFAULT NULL COMMENT 'user id',
	`activation_type` tinyint(1) DEFAULT NULL COMMENT '1 email, 2 api',
	UNIQUE KEY(`id`)
) ENGINE=MyISAM	DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT 'user activation data';

