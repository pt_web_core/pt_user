SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------
--
-- Table structure for table `ipv6_location_user_correction`
--
-- ------------------------------------------------------
CREATE TABLE `ipv6_location_user_correction` (
  `ip` decimal(39,0) UNSIGNED DEFAULT NULL COMMENT 'IP LONG INT',
  `country_code` char(2) COLLATE utf8_bin DEFAULT NULL COMMENT 'Country Code XX',
  `country_name` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'Country Name in English',
  `region_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'State/Province/Region',
  `city_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'City/Town',
  `latitude` double DEFAULT NULL COMMENT 'Latitude Approximation',
  `longitude` double DEFAULT NULL COMMENT 'Longitude Approximation'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Ipv6 Address Ranges to Location Corrections';
-- ------------------------------------------------------
--
-- Indexes for table `ipv6_location_user_correction`
--
-- ------------------------------------------------------
ALTER TABLE `ipv6_location_user_correction`
  ADD UNIQUE KEY `ip_2` (`ip`),
  ADD KEY `ip` (`ip`);
-- ------------------------------------------------------