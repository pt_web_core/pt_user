<?php

/**
 * DB_HOST: database host, usually it's "127.0.0.1" or "localhost", some servers also need port info
 * DB_NAME: name of the database. please note: database and database table are not the same thing
 * DB_USER: user for your database. the user needs to have rights for SELECT, UPDATE, DELETE and INSERT.
 * DB_PASS: the password of the above user.
 */

define("DB_HOST", "127.0.0.1");
define("DB_NAME", "pt");
define("DB_USER", "pt");
define("DB_PASS", "pt");

/**
 * COMPANY_NAME:
 * DOMAIN_NAME:
 * DOMAIN_MAILER: 
 * SITE_UNIQUE_KEY: The key to lock data to site.
 */
define("COMPANY_NAME", "Example Inc.");
define("DOMAIN_NAME", "example.com");
define("DOMAIN_MAILER", "no-reply@".DOMAIN_NAME);

define("SITE_UNIQUE_KEY", "0x100000000");
define("PASSWORD_HASHING", PASSWORD_ARGON2I);
 
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
?>