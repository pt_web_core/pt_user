<?php

/**
 * Define HTML tags for Web UI.
 */

define("OPEN_BODY", "<body>");
define("CLOSE_BODY", "</body>");

define("OPEN_LIST_ITEM", "<li>");
define("CLOSE_LIST_ITEM", "</li>");
//----------------------------------------------------------//
define("OPEN_PAGE", "<page>");
define("CLOSE_PAGE", "</page>"); 

define("OPEN_BOX", "<box>");
define("CLOSE_BOX", "</box>"); 

define("OPEN_ERROR", "<error>");
define("CLOSE_ERROR", "</error>"); 

define("OPEN_CAUTION", "<caution>");
define("CLOSE_CAUTION", "</caution>"); 

define("OPEN_GOOD", "<good>");
define("CLOSE_GOOD", "</good>"); 
?>